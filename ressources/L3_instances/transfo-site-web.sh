#/bin/sh

# transformation (dénotée -->) des données de numéro et durée de créneau pour visualisation avec le site web
# [slotsPerDay=5 <=> 8 créneaux de 1h20 par journée] --> [slotsPerDay=288 <=> créneau de durée unitaire 5 minutes]
# [start=1 <=> créneau de 8h-9h30] --> [start=96] 
# [start=2 <=> créneau de 8h-9h30] --> [start=115]
# ... 

cat edt-problem-1819-l3info-s2.xml | sed -E -e "s/slotsPerDay=\"5\"/slotsPerDay=\"288\"/" | sed -E -e "s/length=\"1\"/length=\"16\"/" | sed -E -e "s/start=\"1\"/start=\"97\"/"| sed -E -e "s/start=\"2\"/start=\"115\"/"| sed -E -e "s/start=\"3\"/start=\"133\"/"| sed -E -e "s/start=\"4\"/start=\"169\"/"| sed -E -e "s/start=\"5\"/start=\"187\"/" > problem.xml

cat problem_ajuste_reduit.xml | sed -E -e
"s/slotsPerDay=\"5\"/slotsPerDay=\"288\"/" | sed -E -e "s/length=\"1\"/length=\"16\"/" | sed -E -e "s/start=\"1\"/start=\"97\"/"| sed -E -e "s/start=\"2\"/start=\"115\"/"| sed -E -e "s/start=\"3\"/start=\"133\"/"| sed -E -e "s/start=\"4\"/start=\"169\"/"| sed -E -e "s/start=\"5\"/start=\"187\"/" > problem_ajuste.xml


cat problem_ajuste.xml | sed -E -e "s/slotsPerDay=\"288\"/slotsPerDay=\"5\"/" | sed -E -e "s/length=\"16\"/length=\"1\"/" | sed -E -e "s/start=\"97\"/start=\"1\"/"| sed -E -e "s/start=\"115\"/start=\"2\"/"| sed -E -e "s/start=\"133\"/start=\"3\"/"| sed -E -e "s/start=\"169\"/start=\"4\"/"| sed -E -e "s/start=\"187\"/start=\"5\"/" > problem_ajuste_reduit.xml

