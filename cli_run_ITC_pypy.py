import datetime
import logging
import pickle
import random
import time

import click

from edt.instance import get_instance
from edt.local_search import local_search

output_directory = "output_tests_ITC_pypy"

logging.basicConfig(
    filename=output_directory + "/execution",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
)


@click.command()
@click.option("--file", help="itc file", type=str)
@click.option("--randseed", help="random seed", type=int)
def main(file, randseed):
    print(file)
    ins = get_instance("ressources/datasetITC2019/" + file, light=True)
    for rand_init, recuit in [(True, True), (False, True), (False, False)]:
        for i in range(randseed):
            print(datetime.datetime.now().strftime("%H:%M:%S"))
            print(f"\trand_init : {rand_init} " f"recuit : {recuit} exec {i}")
            random.seed(i)
            start_time = time.time()
            scores = local_search(ins, rand_init=rand_init, recuit=recuit)
            t = time.time() - start_time
            logging.info(f"{randseed} {rand_init} {recuit} {t} {ins.problem.name}")
            h = t // 3600
            s = t - (h * 3600)
            m = s // 60
            s = s - (m * 60)
            print(
                f"\tExecution time : {int(h):02}:{int(m):02}:{int(s):02} "
                f"{scores[-1]}"
            )

            with open(
                f"{output_directory}/{randseed}_{rand_init}_{recuit}_"
                f"{ins.problem.name}.pck",
                "wb",
            ) as f:
                pickle.dump(scores, f)
            for c in ins.all_classes:
                c.room = None
                c.time = None


if __name__ == "__main__":
    main()
