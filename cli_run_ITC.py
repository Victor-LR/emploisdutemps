import datetime
import logging
import pickle
import random
import time

import click

from edt.instance import get_instance
from edt.local_search import local_search

logging.basicConfig(
    filename="output_tests_ITC/execution",
    level=logging.INFO,
    format="%(asctime)s %(message)s",
)


@click.command()
@click.option("--file", help="itc file", type=str)
@click.option("--randseed", help="random seed", type=int)
@click.option(
    "--rand_init",
    help="1 random initialisation for local search," " 0 tabou initialisation",
    type=bool,
)
@click.option(
    "--recuit",
    default=False,
    help="1 recuit for local search," " 0 tabou initialisation",
    type=bool,
)
def main(file, randseed, rand_init, recuit):
    random.seed(randseed)
    print(datetime.datetime.now().strftime("%H:%M:%S"))
    ins = get_instance("ressources/datasetITC2019/" + file, light=True)
    start_time = time.time()
    scores = local_search(ins, rand_init=rand_init, recuit=recuit)
    t = time.time() - start_time
    logging.info(f"{randseed} {rand_init} {recuit} {t} " f"{ins.problem.name}")
    h = t // 3600
    s = t - (h * 3600)
    m = s // 60
    s = s - (m * 60)
    print(f"Execution time : {int(h):02}:{int(m):02}:{int(s):02}")

    with open(
        f"output_tests_ITC/{randseed}_{rand_init}_{recuit}_" f"{ins.problem.name}.pck",
        "wb",
    ) as f:
        pickle.dump(scores, f)


if __name__ == "__main__":
    main()
