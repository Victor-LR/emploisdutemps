import pickle
from statistics import mean

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()


def load_scores(file):
    with open(file, "rb") as f:
        return pickle.load(f)


output_directory = "output_tests_ITC_pypy/"

with open(output_directory + "execution") as f:
    content = f.readlines()

files = [
    "tg-fal17.xml",
    "tg-spr18.xml",
    "iku-spr18.xml",
    "lums-spr18.xml",
    "lums-fal17.xml",
    "bet-sum18.xml",
    "lums-sum17.xml",
    "iku-fal17.xml",
    "muni-fspsx-fal17.xml",
    "agh-ggis-spr17.xml",
    "muni-fi-spr17.xml",
    "wbg-fal10.xml",
    "muni-pdfx-fal17.xml",
    "yach-fal17.xml",
    "muni-fsps-spr17c.xml",
    "pu-cs-fal07.xml",
    "muni-pdf-spr16.xml",
    "agh-ggos-spr17.xml",
]

files = map(lambda a: a[:-4], files)

met_temps = ["temps RAND RC", "temps TABOU RC", "temps TABOU"]

met_nom = ["RAND RC", "TABOU RC", "TABOU"]

inst = {nom: {n: [] for n in met_nom + met_temps} for nom in files}

for line in content:
    d, t, rs, rand_init, recuit, exec_time, file = line.split()
    execution_time = float(exec_time)
    sf = "_".join([rs, rand_init, recuit, file]) + ".pck"
    scores = load_scores(output_directory + sf)
    if recuit == "True":
        if rand_init == "True":
            inst[file]["RAND RC"].append(scores)
            inst[file]["temps RAND RC"].append(execution_time)
        else:
            inst[file]["TABOU RC"].append(scores)
            inst[file]["temps TABOU RC"].append(execution_time)
    else:
        inst[file]["TABOU"].append(scores)
        inst[file]["temps TABOU"].append(execution_time)

lignes = [
    "temps min",
    "temps moyen",
    "score strict min",
    "score strict moyen",
    "score souple min",
    "score souple moyen",
]
for n in met_nom:
    print(n, end=", ")
for nom_ins, ins in inst.items():

    print()
    print(nom_ins, end=", ")
    for n in met_nom:
        # print(f'{min(ins["temps " + n]):0.2f}', end=', ')
        print(f'{mean(ins["temps " + n]):0.0f}', end=", ")
        # print(f'{max(ins["temps " + n]):0.2f}', end=', ')
        # print(
        #     f'{min([score[-1].hard for score in ins[n]]):0.2f}',
        #     end=', ')
        print(f"{mean([score[-1].hard for score in ins[n]]):0.0f}", end=", ")
        # print(
        #     f'{max([score[-1].hard for score in ins[n]]):0.2f}',
        #     end=', ')
        # print(
        #     f'{min([score[-1].soft for score in ins[n]]):0.2f}',
        #     end=', ')
        print(f"{mean([score[-1].soft for score in ins[n]]):0.0f}", end=", ")
        # print(f'{max([score[-1].soft for score in ins[n]]):0.2f}',
        #       end=', ')

for nom_ins, ins in inst.items():

    if nom_ins != "muni-fi-spr17":
        continue

    for met in met_nom:

        scores_to_mean_hard = []
        scores_to_mean_soft = []

        for s in ins[met]:
            scores_to_mean_hard.append([sa.hard for sa in s if sa is not None])
            scores_to_mean_soft.append([sa.soft for sa in s if sa is not None])

        len_max = max([len(x) for x in scores_to_mean_hard])
        for x in scores_to_mean_hard:
            len_current = len(x)
            for _ in range(len_current, len_max):
                x.append(x[-1])

        for x in scores_to_mean_soft:
            len_current = len(x)
            for _ in range(len_current, len_max):
                x.append(x[-1])

        arrays = [np.array(x) for x in scores_to_mean_hard]
        ins["mean_hard " + met] = [np.mean(k) for k in zip(*arrays)]
        arrays = [np.array(x) for x in scores_to_mean_soft]
        ins["mean_soft " + met] = [np.mean(k) for k in zip(*arrays)]

    # for met in met_nom:
    # fig, ax = plt.subplots()
    # for s in ins[met]:
    #     if None in s:
    #         plt.axvline(s.index(None), linewidth=1, color='b')
    #         s = s[0:s.index(None)] + s[s.index(None) + 1::]
    #     ax.plot([sa.hard for sa in s], label='Hard', color='r')
    #     ax.plot([sa.soft for sa in s], label='Soft', color='#FFA500')
    # # ax.legend()
    # ax.set_xlabel('tours')
    # ax.set_ylabel('nb contraintes')
    # handles, labels = plt.gca().get_legend_handles_labels()
    # newLabels, newHandles = [], []
    # for handle, label in zip(handles, labels):
    #     if label not in newLabels:
    #         newLabels.append(label)
    #         newHandles.append(handle)
    # plt.title(nom_ins + ' ' + met)
    # plt.xlim(0)
    # plt.ylim(0)
    # plt.legend(newHandles, newLabels, loc='upper right')
    # plt.show()

    fig, (ax0, ax1) = plt.subplots(
        figsize=(8, 6), nrows=2, gridspec_kw={"height_ratios": [1, 1.5]}
    )

    for met in met_nom:
        ax0.plot(ins["mean_hard " + met], label=nom_ins + " " + met)
        ax1.plot(ins["mean_soft " + met], label=nom_ins + " " + met)

    ax0.set_xlim(0)
    ax0.set_ylim(0, 300)  # 5000
    ax1.set_xlim(0)
    ax1.set_ylim(0, 35000)  # presque 100000
    ax0.set_ylabel("contraintes dures")
    ax1.set_ylabel("contraintes souples")
    ax0.set_xlabel("itérations")
    ax1.set_xlabel("itérations")

    ax1.set_title("Contraintes souples")
    ax0.set_title("Contraintes dures")
    # fig.tight_layout()

    handles, labels = plt.gca().get_legend_handles_labels()

    newLabels, newHandles = [], []
    for handle, label in zip(handles, labels):
        if label not in newLabels:
            newLabels.append(label)
            newHandles.append(handle)
    plt.legend(newHandles, newLabels)
    # plt.legend(newHandles, newLabels, bbox_to_anchor=(1.05, 1),
    #            loc='upper left',
    #            borderaxespad=0.)

    plt.show()

    # print(f'{min(liste["times"])=:0.2f}')
    # print(f'{mean(liste["times"])=:0.2f}')
    # print(f'{max(liste["times"])=:0.2f}')
    # print('-' * 10)
    # print(f'{min([score[-1].hard for score in liste["scores"]])=:0.2f}')
    # print(f'{min([score[-1].soft for score in liste["scores"]])=:0.2f}')
    # print(f'{mean([score[-1].hard for score in liste["scores"]])=:0.2f}')
    # print(f'{mean([score[-1].soft for score in liste["scores"]])=:0.2f}')
    # print(f'{max([score[-1].hard for score in liste["scores"]])=:0.2f}')
    # print(f'{max([score[-1].soft for score in liste["scores"]])=:0.2f}')
    # print('=' * 20)
