#!/bin/bash

source /home/user/Documents/venv/bin/activate

files=("tg-fal17.xml" "tg-spr18.xml" "iku-spr18.xml" "lums-spr18.xml"
  "lums-fal17.xml" "bet-sum18.xml" "lums-sum17.xml" "iku-fal17.xml"
  "muni-fspsx-fal17.xml" "agh-ggis-spr17.xml" "muni-fi-spr17.xml"
  "wbg-fal10.xml" "muni-pdfx-fal17.xml" "yach-fal17.xml" "muni-fsps-spr17c.xml"
  "pu-cs-fal07.xml" "muni-pdf-spr16.xml" "agh-ggos-spr17.xml")

for file in "${files[@]}"; do
  pypy3 get_csv.py --file $file
done
