"""
Gestion de l'instance traitée
"""
import os
import sys
import re
import signal
import platform
from collections import Counter
from os import system
from typing import Dict, List, Set, Tuple, Optional
from xml.etree.ElementTree import parse, Element, SubElement, tostring

import networkx as nx
from termcolor import colored

from edt.distributions import Distribution, PseudoSameAttendees
from edt.instance_objects import (
    Class,
    Config,
    Course,
    Optimization,
    Problem,
    Room,
    Student,
    StudentGroup,
    Subpart,
    Time,
)
from edt.parse_xml_objects import parse_file_object

if platform.python_implementation() == "CPython":
    import matplotlib.pyplot as plt

instance_global = None


class Score:
    """Représentation du score"""

    # pylint: disable=too-many-instance-attributes

    def __init__(
        self,
        student_clashes: int,
        room_clashes: int,
        hard_clashes: int,
        soft_clashes: int,
        room_penalties: int,
        time_penalties: int,
    ):
        """Constructeur de score

        Args:
            student_clashes (int): contraintes sur les étudiants
            room_clashes (int): contraintes sur les salles
            hard_clashes (int): contraintes de répartition dures
            soft_clashes (int): contraintes de répartition souples
            room_penalties (int): pénalités sur les salles
            time_penalties (int): pénalités sur les créneaux
        """
        global instance_global
        opti = instance_global.optimization
        self.student_clashes = student_clashes * opti.student
        self.room_clashes = room_clashes
        self.hard_clashes = hard_clashes
        self.soft_clashes = soft_clashes * opti.distribution
        self.room_penalties = room_penalties * opti.room
        self.time_penalties = time_penalties * opti.time
        self.hard = self.room_clashes + self.hard_clashes
        self.soft = (
            self.soft_clashes
            + self.room_penalties
            + self.time_penalties
            + self.student_clashes
        )

    def __repr__(self) -> str:
        """Représentation du score

        Returns:
            str: texte à afficher
        """
        hard = colored(
            f"{self.hard:3} (room : {self.room_clashes},"
            f" distri : {self.hard_clashes}) ",
            "red",
        )
        soft = colored(
            f"{self.soft:3} (room : {self.room_penalties}, "
            f"distri : {self.soft_clashes}, "
            f"time : {self.time_penalties}, "
            f"students : {self.student_clashes})",
            "magenta",
        )
        return hard + soft
        # return f'HARD : {self.hard:3}, SOFT : {self.soft:3} ' \
        #        f'(student_clash : {self.student_clashes}' \
        #        f', room_clash : {self.room_clashes}' \
        #        f', hard_distri : {self.hard_clashes}' \
        #        f', soft_distri : {self.soft_clashes}' \
        #        f', room_penalt : {self.room_penalties}' \
        #        f', time_penalt : {self.time_penalties})'

    def __eq__(self, other: any) -> bool:
        """Retourne vrai si deux scores sont égaux

        Args:
            other (any): l'autre score

        Returns:
            bool: vrai si les scores sont égaux
        """
        if self.__class__ == other.__class__:
            return (
                self.student_clashes == other.student_clashes
                and self.room_clashes == other.room_clashes
                and self.hard_clashes == other.hard_clashes
                and self.soft_clashes == other.soft_clashes
                and self.room_penalties == other.room_penalties
                and self.time_penalties == other.time_penalties
            )
        return False

    def __hash__(self) -> int:
        """hash le score

        Returns:
            int: valeur de hashage
        """
        return hash(
            (
                self.student_clashes,
                self.room_clashes,
                self.hard_clashes,
                self.soft_clashes,
                self.room_penalties,
                self.time_penalties,
            )
        )

    def str_soft(self) -> str:
        """Affichage des contraintes souples

        Returns:
            str: texte des contraintes souples
        """
        return (
            f"SOFT : {self.soft:3} "
            f"(soft_clashes : {self.soft_clashes}"
            f", room_penalties : {self.room_penalties}"
            f", time_penalties : {self.time_penalties})"
        )


class Instance:
    """Représente une instance du problème
    Attention, ne pas gérer deux instances à la fois car
    les créneaux, salles, séances et divers paramètres
    disposent d'un compteur d'instance.
    """

    def __init__(
        self,
        problem: Problem,
        optimization: Optimization,
        rooms: Dict[str, Room],
        courses: Dict[str, Course],
        distributions: List[Distribution],
        students: Dict[str, Student],
        light: bool = False,
    ):
        """Constructeur d'instance

        Args:
            problem (Problem): problème
            optimization (Optimization): poids du problème
            rooms (Dict[str, Room]): ensemble des salles
            courses (Dict[str, Course]): ensemble des cours
            distributions (List[Distribution]): ensemble des contraintes
            students (Dict[str, Student]): ensemble des étudiants
            light (bool, optional): True si student sectioning avant recherche.
            Defaults to False.
        """
        global instance_global
        instance_global = self
        self.problem: Problem = problem
        self.optimization: Optimization = optimization
        self.rooms: Dict[str, Room] = rooms
        self.courses: Dict[str, Course] = courses
        self.distributions: List[Distribution] = distributions
        self.students: Dict[str, Student] = students
        for _, s in self.students.items():
            for c in s.course_id:
                c.registered.append(s)
        self.unique_students: Set[StudentGroup] = set()
        self.reduced_students = []
        self.meta_students = []
        self.unique_group = []
        self.group = []
        self.reduce_number_students()
        # self.generate_meta_students()
        self.all_classes: List[Class] = []
        # self.min_timeslot = 288
        # self.max_timeslot = 0
        self.all_subparts: List[Subpart] = []
        self.all_configs: List[Config] = []
        for _, course in self.courses.items():
            for _, config in course:
                self.all_configs.append(config)
                for _, subpart in config:
                    self.all_subparts.append(subpart)
                    for _, classe in subpart:
                        self.all_classes.append(classe)
        #                 for t in classe.times:
        #                     if t.start < self.min_timeslot:
        #                         self.min_timeslot = t.start
        #                     if t.start > self.max_timeslot:
        #                         self.max_timeslot = t.start
        # a = (self.max_timeslot - self.min_timeslot)
        # self.nb_timeslot = a * self.problem.nrDays * self.problem.nrWeeks
        self.all_times = [t for c in self.all_classes for t in c.times]
        self.all_times.sort(key=lambda t: t.int_id)
        self.all_una = [un for _, room in self.rooms.items() for un in room.unavailable]
        self.all_una.sort(key=lambda t: t.int_id)
        self.all_rooms = list(self.rooms)
        self.all_students = list(self.students.values())
        self.xclass = None
        self.last_cost = 99999
        self.scores = []
        self.score_context = []
        self.rencontres = None
        self.rencontres_score = None
        for d in distributions:
            d.problem = self.problem

        instance_for_handler = self
        inscriptions_file = (
            f"ressources/inscriptions/" f"inscription_{self.problem.name}.xml"
        )

        self.same_attends: List[PseudoSameAttendees] = []
        if light:
            from edt.student_sectionning import (
                make_group_from_courses,
                save_inscriptions_xml,
                load_inscriptions_xml,
            )

            if os.path.exists(inscriptions_file):
                load_inscriptions_xml(self, inscriptions_file)
            else:
                make_group_from_courses(self)
                save_inscriptions_xml(self)
            self.convert_to_sameattendees()

        def receive_signal(signalNumber, frame):
            print("Stop with", signalNumber, "on", frame)
            instance_for_handler.get_score(context="handler", verbose=True)
            # if self.problem.name == "week10-room5-dist1":
            #     from edt.web import launch_on_web
            #     launch_on_web(self)
            sys.exit(0)

        signal.signal(signal.SIGINT, receive_signal)
        self.mzn_inst = None

    def __repr__(self) -> str:
        """Représentation de l'instance

        Returns:
            str: texte à afficher
        """
        s = repr(self.problem) + repr(self.optimization)
        for _, r in self.rooms.items():
            s += repr(r) + "\n"
        for _, c in self.courses.items():
            s += repr(c) + "\n"
        for d in self.distributions:
            s += repr(d) + "\n"
        for _, stu in self.students.items():
            s += repr(stu) + "\n"
        # s += f'timeslot min : {self.min_timeslot},' \
        #      f'timeslot max : {self.max_timeslot}\n' \
        #      f'#timeslots : {self.nb_timeslot}'
        return s

    def str_current_solution(self) -> str:
        """Affichage de la solution courante

        Returns:
            str: texte à afficher
        """
        s = repr(self.problem) + repr(self.optimization)
        for c in self.all_classes:
            s += f"{c}\n\t{c.room}\n\t{c.time}\n"
        return s

    def get_score(
        self,
        context: Optional[str] = None,
        verbose: bool = False,
        save_score: bool = True,
    ) -> Score:
        """Calcul du score de la solution courante, toutes les séances doivent être placées.

        Args:
            context (Optional[str], optional): contexte du calcul. Defaults to None.
            verbose (bool, optional): True si affichage du score. Defaults to False.
            save_score (bool, optional): TRue si enregistrement du score.
            Defaults to True.

        Returns:
            Score: [description]
        """
        for c in self.all_classes:
            c.reset_counter()
        problem_same_attends = []
        for i, sa in enumerate(self.same_attends):
            sa.check_constraint(problem_same_attends)
        problems_rooms: List[Tuple[Class, Class]] = []
        problems_rooms2: List[Class] = []
        for r in self.rooms.values():
            if r != self.rooms["noroom"]:
                for c1 in range(len(r.users)):
                    cl1 = r.users[c1]
                    if any([cl1.time.overlap(una) for una in r.unavailable]):
                        cl1.room_clashes += 1
                        problems_rooms2.append(cl1)
                    for c2 in range(c1 + 1, len(r.users)):
                        cl2 = r.users[c2]
                        if cl1.time.overlap(cl2.time):
                            cl1.room_clashes += 1
                            cl2.room_clashes += 1
                            problems_rooms.append((cl1, cl2))
        # for c1 in range(len(self.all_classes)):
        #     for c2 in range(c1 + 1, len(self.all_classes)):
        #         cc1 = self.all_classes[c1]
        #         cc2 = self.all_classes[c2]
        #         room_c1 = cc1.room[0]
        #         room_c2 = cc2.room[0]
        #         if room_c1 == room_c2:
        #             if room_c1 != self.rooms['noroom']:
        #                 time_c1 = cc1.time
        #                 time_c2 = cc2.time
        #                 if not time_c1 != time_c2:
        #                     problems_rooms.append((cc1, cc2))
        #                     cc1.room_clashes += 1
        #                     cc2.room_clashes += 1
        distri = [False] * len(self.distributions)
        for i, d in enumerate(self.distributions):
            distri[i] = d.check_constraint()
        student_clashes = sum([c.student_clashes for c in self.all_classes])
        room_clashes = sum([c.room_clashes for c in self.all_classes])
        hard_clashes = sum([c.hard_distribution_clashes for c in self.all_classes])
        soft_clashes = sum([c.soft_distribution_clashes for c in self.all_classes])
        room_penalties = sum([c.room[1] for c in self.all_classes])
        time_penalties = sum([c.time.penalty for c in self.all_classes])
        score = Score(
            student_clashes,
            room_clashes,
            hard_clashes,
            soft_clashes,
            room_penalties,
            time_penalties,
        )
        if save_score:
            self.scores.append(score)
            self.score_context.append(context)
        if verbose:
            s = ""
            if score.hard == 0:
                s += colored("0 hard problem\n", "green")
            else:
                no = f"HARD : {score.hard}\n"
                if any(distri):
                    for bad, distri in zip(distri, self.distributions):
                        if not bad:
                            no += str(distri) + "\n"
                if len(problem_same_attends) != 0:
                    for c1, c2 in problem_same_attends:
                        no += f"{c1.id} and {c2.id} same attendees\n"
                if len(problems_rooms) != 0:
                    for c1, c2 in problems_rooms:
                        no += f"{c1.id} and {c2.id} in the same room\n"
                if len(problems_rooms2) != 0:
                    for c1 in problems_rooms2:
                        no += f"{c1.id}'s room unavailable\n"
                s += colored(no, "red")
            s += colored(score.str_soft(), "magenta")
            print(s)
        return score

    def get_partial_score(
        self,
        context: Optional[str] = None,
        verbose: bool = False,
        save_score: bool = True,
    ) -> Score:
        """Calcul du score sur toutes les séances en prenant en compte les séances non
        placées sur le planning

        Args:
            context (Optional[str], optional): contexte du calcul (ls, tabou,...).
            Defaults to None.
            verbose (bool, optional): True si affichage des résultats.
            Defaults to False.
            save_score (bool, optional): True si sauvegarde du score. Defaults to True.

        Returns:
            Score: score calculé
        """
        for c in self.all_classes:
            c.reset_counter()
        problem_same_attends = []
        for i, sa in enumerate(self.same_attends):
            sa.check_partial_constraint(problem_same_attends)
        problems_rooms = []
        problems_rooms2 = []
        for r in self.rooms.values():
            if r != self.rooms["noroom"]:
                for c1 in range(len(r.users)):
                    cl1 = r.users[c1]
                    if cl1.time is not None:
                        if any([cl1.time.overlap(una) for una in r.unavailable]):
                            cl1.room_clashes += 1
                            problems_rooms2.append(cl1)
                        for c2 in range(c1 + 1, len(r.users)):
                            cl2 = r.users[c2]
                            if cl2.time is not None:
                                if cl1.time.overlap(cl2.time):
                                    cl1.room_clashes += 1
                                    cl2.room_clashes += 1
                                    problems_rooms.append((cl1, cl2))
        # for c1 in range(len(self.all_classes)):
        #     for c2 in range(c1 + 1, len(self.all_classes)):
        #         cc1 = self.all_classes[c1]
        #         cc2 = self.all_classes[c2]
        #         if cc1.time is not None and cc2.time is not None and \
        #                 cc1.room is not None and cc2.room is not None:
        #             room_c1 = cc1.room[0]
        #             room_c2 = cc2.room[0]
        #             if room_c1 == room_c2:
        #                 if room_c1 != self.rooms['noroom']:
        #                     time_c1 = cc1.time
        #                     time_c2 = cc2.time
        #                     if not time_c1 != time_c2:
        #                         problems_rooms.append((cc1, cc2))
        #                         cc1.room_clashes += 1
        #                         cc2.room_clashes += 1
        distri = [False] * len(self.distributions)
        for i, d in enumerate(self.distributions):
            distri[i] = d.check_partial_constraint()
        student_clashes = sum([c.student_clashes for c in self.all_classes])
        room_clashes = sum([c.room_clashes for c in self.all_classes])
        hard_clashes = sum([c.hard_distribution_clashes for c in self.all_classes])
        soft_clashes = sum([c.soft_distribution_clashes for c in self.all_classes])
        room_penalties = sum(
            [c.room[1] for c in self.all_classes if c.room is not None]
        )
        time_penalties = sum(
            [c.time.penalty for c in self.all_classes if c.time is not None]
        )
        score = Score(
            student_clashes,
            room_clashes,
            hard_clashes,
            soft_clashes,
            room_penalties,
            time_penalties,
        )
        if save_score:
            self.scores.append(score)
            self.score_context.append(context)
        if verbose:
            s = ""
            if score.hard == 0:
                s += colored("0 hard problem\n", "green")
            else:
                no = f"HARD : {score.hard}\n"
                if any(distri):
                    for bad, distri in zip(distri, self.distributions):
                        if not bad:
                            no += str(distri) + "\n"
                # if len(problems_students) != 0:
                #     for c1, c2 in problems_students:
                #         no += f'{c1.id} and {c2.id} overlaps\n'
                if len(problem_same_attends) != 0:
                    for c1, c2 in problem_same_attends:
                        no += f"{c1.id} and {c2.id} same attendees\n"
                if len(problems_rooms) != 0:
                    for c1, c2 in problems_rooms:
                        no += f"{c1.id} and {c2.id} in the same room\n"
                if len(problems_rooms2) != 0:
                    for c1 in problems_rooms2:
                        no += f"{c1.id}'s room unavailable\n"
                s += colored(no, "red")
            s += colored(score.str_soft(), "magenta")
            print(s)
        return score

    def get_partial_score_class(self, classe: Class, verbose: bool = False) -> Score:
        """Calcul du score pour une séance en prenant en compte les séances non placée

        Args:
            classe (Class): la séance à analyser
            verbose (bool, optional): afficher les résultats. Defaults to False.

        Returns:
            Score: score obtenu
        """
        classe.reset_counter()
        problem_same_attends = []
        for sa in classe.in_peusdo_same_attendees:
            sa.check_partial_constraint(problem_same_attends)
        problems_rooms = []
        problems_rooms2 = []
        if any([classe.time.overlap(una) for una in classe.room[0].unavailable]):
            classe.room_clashes += 1
            problems_rooms2.append(classe)
        if classe.room[0] != self.rooms["noroom"]:
            for c1 in range(len(classe.room[0].users)):
                for c2 in range(c1 + 1, len(classe.room[0].users)):
                    cl1 = classe.room[0].users[c1]
                    cl2 = classe.room[0].users[c2]
                    if cl1.time is not None and cl2.time is not None:
                        if cl1.time.overlap(cl2.time):
                            cl1.room_clashes += 1
                            cl2.room_clashes += 1
                            problems_rooms.append((cl1, cl2))
        # cc1 = classe
        # for cc2 in self.all_classes:
        #     if cc2.int_id == cc1.int_id:
        #         continue
        #     if cc1.time is not None and cc2.time is not None and \
        #             cc1.room is not None and cc2.room is not None:
        #         room_c1 = cc1.room[0]
        #         room_c2 = cc2.room[0]
        #         if room_c1 == room_c2:
        #             if room_c1 != self.rooms['noroom']:
        #                 time_c1 = cc1.time
        #                 time_c2 = cc2.time
        #                 if not time_c1 != time_c2:
        #                     problems_rooms.append((cc1, cc2))
        #                     cc1.room_clashes += 1
        #                     cc2.room_clashes += 1
        distri = [False] * len(classe.in_distri)
        for i, d in enumerate(classe.in_distri):
            distri[i] = d.check_partial_constraint()
        student_clashes = classe.student_clashes
        room_clashes = classe.room_clashes
        hard_clashes = classe.hard_distribution_clashes
        soft_clashes = classe.soft_distribution_clashes
        room_penalties = classe.room[1]
        time_penalties = classe.time.penalty
        score = Score(
            student_clashes,
            room_clashes,
            hard_clashes,
            soft_clashes,
            room_penalties,
            time_penalties,
        )
        if verbose:
            s = ""
            if score.hard == 0:
                s += colored("0 hard problem\n", "green")
            else:
                no = f"HARD : {score.hard}\n"
                if any(distri):
                    for bad, distri in zip(distri, self.distributions):
                        if not bad:
                            no += str(distri) + "\n"
                if len(problem_same_attends) != 0:
                    for c1, c2 in problem_same_attends:
                        no += f"{c1.id} and {c2.id} same attendees\n"
                if len(problems_rooms) != 0:
                    for c1, c2 in problems_rooms:
                        no += f"{c1.id} and {c2.id} in the same room\n"
                s += colored(no, "red")
            s += colored(score.str_soft(), "magenta")
            print(s)
        return score

    def find_class_by_id(self, class_id: str) -> Optional[Class]:
        """Retourne la séance demandée, None si séance non trouvée

        Args:
            class_id (str): La séance à chercher

        Returns:
            Optional[Class]: séance trouvée ou None
        """
        for c in self.all_classes:
            if c.id == class_id:
                return c
        return None

    def get_xml_solution(self) -> None:
        """Création du fichier solution"""

        solution = Element(
            "solution",
            {
                "name": self.problem.name,
                "author": "moi",
                "country": "France",
                "institution": "UA",
                "runtime": "some time",
                "technique": "magic",
            },
        )
        for c in self.all_classes:
            attribs = {
                "id": str(c.id),
                "days": "".join(
                    [
                        "1" if i in c.time.days else "0"
                        for i in range(self.problem.nrDays)
                    ]
                ),
                "start": str(c.time.start),
                "weeks": "".join(
                    [
                        "1" if i in c.time.weeks else "0"
                        for i in range(self.problem.nrWeeks)
                    ]
                ),
                "room": str(c.room[0].id),
            }
            child = SubElement(solution, "class", attribs)
            for s in c.registered:
                SubElement(child, "student", {"id": s.id})
        sol_file = f"ressources/solutions/PROJET-solution_{self.problem.name}.xml"
        with open(sol_file, "w") as xml_f:
            xml_f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
            xml_f.write(
                "<!DOCTYPE solution PUBLIC "
                '"-//UA 2020//DTD Solution Format/EN" '
                '"edt-format-solution.dtd">'
            )
            xml_f.write(tostring(solution).decode("utf8"))
        system(f"cat {sol_file} |" f" xmlstarlet format --indent-tab > {sol_file}_tmp")
        system(f"mv {sol_file}_tmp {sol_file}")

    def read_solution(self, solution_file: str):
        """Charge un fichier solution

        Args:
            solution_file (str): fichier à charger
        """
        tree = parse(solution_file)
        solution = tree.getroot()

        if solution.attrib["name"] != self.problem.name:
            print("Solution file and instance have different names")

        for child in solution:
            if child.tag == "class":
                class_name = child.attrib["id"]
                days = [m.start() for m in re.finditer("1", child.attrib["days"])]
                start = int(child.attrib["start"])
                weeks = [m.start() for m in re.finditer("1", child.attrib["weeks"])]
                room = child.attrib["room"]
                studs = [
                    self.students[stud.attrib["id"]]
                    for stud in child
                    if stud.tag == "student"
                ]
                real_class = [c for c in self.all_classes if c.id == class_name][0]
                for t in real_class.times:
                    if t.days == days and t.weeks == weeks and t.start == start:
                        real_class.time = t
                for r in real_class.classrooms:
                    if r[0].id == room:
                        real_class.room = r
                real_class.remove_all_students()
                for s in studs:
                    real_class.add_student(s)
        self.generate_xclass()

    def get_classes_using_room(self, room: Room, times=None) -> List[Class]:
        """Retourne la liste des séances utilisant la salle de classe room

        Args:
            room (Room): la salle de classe
            times (Time, optional): horaires d'utilisation. Defaults to None.

        Returns:
            List[Class]: liste des séances utilisant la salle
        """
        classes = []
        for c in self.all_classes:
            if c.room[0] == room:
                if times is not None:
                    if any([c.time.same_time(t) for t in times]):
                        classes.append(c)
                    else:
                        classes.append(c)
        return classes

    def reduce_number_students(self) -> None:
        """Incorpore des groupes d'étudiants avec moins de cours dans
        des groupes plus importants. Fonction pas fiable si un groupe
        peut-être contenus dans deux groupes différents
        """
        for s in self.students.values():
            if s in self.unique_students:
                for us in self.unique_students:
                    if s == us:
                        us.group.append(s)
            else:
                self.unique_students.add(StudentGroup(s))
            added = False
            for rs in self.reduced_students:
                if not added:
                    include = s.include(rs)
                    if include == 1:
                        rs.group.append(s)
                        added = True
                    elif include == -1:
                        rs.course_id = s.course_id
                        rs.group.append(s)
                        added = True
            if not added:
                self.reduced_students.append(StudentGroup(s))

    def generate_unique_group(self) -> None:
        """Génération des groupes d'uniques students"""
        for us in self.unique_students:
            added = None
            to_delete = []
            for g in self.unique_group:
                if any(
                    [set(us.course_id).intersection(sg.course_id) != set() for sg in g]
                ):
                    if added is not None:
                        to_delete.append(g)
                        added.extend(g)
                    else:
                        g.append(us)
                        added = g
            if not added:
                self.unique_group.append([us])
            for g in to_delete:
                self.unique_group.remove(g)
        self.unique_group.sort(key=lambda x: (len(x), sum([len(y.group) for y in x])))

    def make_bipartite_graph(self) -> None:
        """Affichages des graphes bipartis (profil, cours)"""
        if platform.python_implementation() == "CPython":
            for g in self.unique_group:
                list_stu = ["S" + stu.group[0].id for stu in g]
                list_cour = [
                    cour.id.replace(" ", "\n") for grp in g for cour in grp.course_id
                ]
                print(Counter(list_cour))
                cours = list(Counter(list_cour).items())
                cours.sort(key=lambda e: e[1], reverse=True)
                cours = list(zip(*cours))[0]
                print(cours)
                list_cour = list(set(list_cour))
                B = nx.Graph()
                B.add_nodes_from(list_stu, bipartite=0)
                B.add_nodes_from(list_cour, bipartite=1)
                for stu in g:
                    for cour in stu.course_id:
                        B.add_edge("S" + stu.group[0].id, cour.id.replace(" ", "\n"))
                top = nx.bipartite.sets(B)[0]
                pos = nx.bipartite_layout(B, top)
                plt.figure(figsize=(10, 10))
                plt.subplots(constrained_layout=False)
                nx.draw(
                    B,
                    pos=pos,
                    with_labels=True,
                    font_size=8,
                    width=0.8,
                    node_size=50,
                    node_color="#999999",
                    edge_color="#999999",
                )
                plt.show()

    def delete_time_for_class(self, class_id: str) -> None:
        """Suppression d'un horaire de cours pour une séance

        Args:
            class_id (str): séance consernée
        """
        classe = self.find_class_by_id(class_id)
        t = classe.time
        classe.time = None
        classe.times.remove(t)
        possibilities = []
        for i, t in enumerate(classe.times):
            classe.time = t
            print("*" * 80)
            print("TIME", i, ":", t)
            r = self.get_score(verbose=True, save_score=False)
            possibilities.append((i, t, r))
        possibilities.sort(key=lambda x: x[2].hard)
        for i, t, r in possibilities:
            print(i, t, r)

    def freeze_all_excluding(self, excluding: List[Tuple[Class, bool, bool]]) -> None:
        """Gel des salles et/ou créneaux sauf pour les classes dans excluding

        Args:
            excluding (List[Tuple[Class, bool, bool]]): liste des classes à ne pas geler
        """
        excluding_classes, rooms, times = zip(*excluding)
        for c in self.all_classes:
            if c in excluding_classes:
                i = excluding_classes.index(c)
                if times[i]:
                    c.times_for_search = [c.time]
                else:
                    c.times_for_search = c.times
                if rooms[i]:
                    c.rooms_for_search = [c.room]
                else:
                    c.rooms_for_search = c.classrooms
            else:
                c.times_for_search = [c.time]
                c.rooms_for_search = [c.room]

    def delete_times_for_room(self, room: str, times: List[Time]) -> None:
        """Ajout d'une indisponibilité pour une salle

        Args:
            room (str): salle concernée
            times (List[Time]): temps d'indisponibilité
        """
        room = self.rooms[room]
        print(room)
        classes = self.get_classes_using_room(room, times)

        for t in times:
            room.unavailable.append(t)
        for c in classes:
            print(c)
            print(c.time)

        excluding = [(c, False, False) for c in classes]

        self.freeze_all_excluding(excluding)
        from edt.ppc import launch_mzn

        launch_mzn(self, "all", xclass=self.xclass)

    def swap_teachers(self, id_c1: str, id_c2: str) -> None:
        """Échange de séances entre enseignants

        Args:
            id_c1 (str): 1er cours
            id_c2 (str): 2nd cours
        """
        c1 = self.find_class_by_id(id_c1)
        print(c1.in_distri)
        c2 = self.find_class_by_id(id_c2)
        print(c2.in_distri)

        d_c1 = None
        for d in c1.in_distri:
            if d.type == "SameAttendees":
                d_c1 = d
        d_c2 = None
        for d in c2.in_distri:
            if d.type == "SameAttendees":
                d_c2 = d

        d_c1.classes.remove(c1)
        d_c2.classes.remove(c2)
        c1.in_distri.remove(d_c1)
        c2.in_distri.remove(d_c2)
        d_c1.classes.append(c2)
        d_c2.classes.append(c1)
        c1.in_distri.append(d_c2)
        c2.in_distri.append(d_c1)

        excluding = [(c1, False, False), (c2, False, False)]
        self.freeze_all_excluding(excluding)
        from edt.ppc import launch_mzn

        launch_mzn(self, "all", xclass=self.xclass)

    def generate_xclass(self) -> None:
        """Récupération des informations d'inscriptions aux séances"""
        self.xclass = []
        for s in self.all_students:
            line = []
            for subp in self.all_subparts:
                inter = set(s.classes).intersection(subp.classes.values())
                if len(inter) != 0:
                    c = inter.pop()
                    line.append(self.all_classes.index(c) + 1)
                else:
                    line.append(0)
            self.xclass.append(line)

    def convert_to_sameattendees(self) -> None:
        """Convertion des inscriptions en contraintes PseudoSameAttendees"""
        sameattendees = []
        for s in self.all_students:
            sameattendees.append(tuple(list(c for c in s.classes)))
        same_attends = Counter(sameattendees)
        for t, n in same_attends.items():
            self.same_attends.append(PseudoSameAttendees(n, t))
        for sa in self.same_attends:
            for c in sa.classes:
                c.in_peusdo_same_attendees.append(sa)

    def generate_instance_mzn(self, light: bool = False) -> None:
        """Récupérations des informations pour la recherche en minizinc

        Args:
            light (bool, optional): True si student sectioning avant la recherche.
            Defaults to False.
        """
        self.mzn_inst = dict()
        self.all_times = [t for c in self.all_classes for t in c.times_for_search]
        self.all_times.sort(key=lambda t: t.int_id)
        for i in range(len(self.all_times)):
            self.all_times[i].id_for_search = i + 1
        self.all_una = [un for _, room in self.rooms.items() for un in room.unavailable]
        for i, u in enumerate(self.all_una):
            u.id_for_search = i + 1
        self.all_una.sort(key=lambda t: t.id_for_search)
        self.all_rooms = list(self.rooms)
        self.mzn_inst["nr_weeks"] = self.problem.nrWeeks
        self.mzn_inst["nr_days_per_week"] = self.problem.nrDays
        self.mzn_inst["nr_slots_per_day"] = self.problem.slotsPerDay
        self.mzn_inst["travel"] = [
            [t[1] for t in r.travel] for _, r in self.rooms.items()
        ]
        if not light:
            self.mzn_inst["nr_courses"] = len(self.courses)
            self.mzn_inst["nr_configs"] = len(self.all_configs)
            self.mzn_inst["nr_subparts"] = len(self.all_subparts)
        self.mzn_inst["nr_classes"] = len(self.all_classes)
        self.mzn_inst["nr_times"] = len(self.all_times)
        self.mzn_inst["nr_rooms"] = len(self.rooms)
        self.mzn_inst["max_capacity"] = max([r.capacity for _, r in self.rooms.items()])
        self.mzn_inst["nr_closures"] = len(
            [t.int_id for _, r in self.rooms.items() for t in r.unavailable]
        )
        self.mzn_inst["nr_students"] = len(self.all_students)
        if light:
            self.mzn_inst["nr_distributions"] = len(self.distributions) + len(
                self.same_attends
            )
        else:
            self.mzn_inst["nr_distributions"] = len(self.distributions)
            self.mzn_inst["configs"] = [
                {conf.int_id for _, conf in c} for _, c in self.courses.items()
            ]
            self.mzn_inst["subparts"] = [
                {sub.int_id for _, sub in conf}
                for _, c in self.courses.items()
                for _, conf in c
            ]
            self.mzn_inst["classes"] = [
                {cla.int_id for _, cla in sub}
                for _, c in self.courses.items()
                for _, conf in c
                for _, sub in conf
            ]
        self.mzn_inst["parent"] = [
            cla.parent.int_id if cla.parent else 0 for cla in self.all_classes
        ]
        self.mzn_inst["limit"] = [cla.limit for cla in self.all_classes]
        self.all_times.sort(key=lambda x: x.penalty, reverse=False)
        for i, t in enumerate(self.all_times):
            t.id_for_search = i + 1

        self.mzn_inst["times"] = [
            {t.id_for_search for t in cla.times_for_search} for cla in self.all_classes
        ]
        self.mzn_inst["time_weeks"] = [
            set(list(map(lambda e: e + 1, t.weeks))) for t in self.all_times
        ]
        self.mzn_inst["time_days"] = [
            set(list(map(lambda e: e + 1, t.days))) for t in self.all_times
        ]
        self.mzn_inst["time_start"] = [t.start for t in self.all_times]
        self.mzn_inst["time_end"] = [t.end for t in self.all_times]
        self.mzn_inst["time_length"] = [int(t.length) for t in self.all_times]
        self.mzn_inst["time_penalty"] = [t.penalty for t in self.all_times]
        self.mzn_inst["rooms"] = [
            {r[0].int_id for r in cla.rooms_for_search} for cla in self.all_classes
        ]
        room_penalty = []
        for cla in self.all_classes:
            room_penalty.append([])
            for _, r in self.rooms.items():
                found = False
                for rc in cla.rooms_for_search:
                    if rc[0].int_id == r.int_id:
                        room_penalty[-1].append(rc[1])
                        found = True
                if not found:
                    room_penalty[-1].append(0)
        self.mzn_inst["room_penalty"] = room_penalty
        self.mzn_inst["virtual_rooms"] = {1}
        self.mzn_inst["capacity"] = [r.capacity for _, r in self.rooms.items()]
        if light:
            self.mzn_inst["student_classes"] = [
                set(c.int_id for c in e.classes) for e in self.all_students
            ]

        self.mzn_inst["closures"] = [
            {t.id_for_search for t in r.unavailable}
            if len(r.unavailable) != 0
            else set()
            for _, r in self.rooms.items()
        ]
        self.mzn_inst["closure_weeks"] = [
            set(list(map(lambda e: e + 1, t.weeks))) for t in self.all_una
        ]
        self.mzn_inst["closure_days"] = [
            set(list(map(lambda e: e + 1, t.days))) for t in self.all_una
        ]
        self.mzn_inst["closure_slot"] = [t.start for t in self.all_una]
        self.mzn_inst["closure_length"] = [t.length for t in self.all_una]
        if light:
            self.mzn_inst["headcount"] = [len(c.registered) for c in self.all_classes]
            self.mzn_inst["distribution"] = [d.nom_mzn for d in self.distributions] + [
                "same_attendees" for _ in self.same_attends
            ]
            self.mzn_inst["distribution_penalty"] = [
                d.penalty if not d.required else 0 for d in self.distributions
            ] + [0 for _ in self.same_attends]

            self.mzn_inst["distribution_classes"] = [
                [c.int_id for c in d.classes]
                + [0] * (len(self.all_classes) - len(d.classes))
                for d in self.distributions
            ] + [
                [c.int_id for c in sa.classes]
                + [0] * (len(self.all_classes) - len(sa.classes))
                for sa in self.same_attends
            ]
        else:
            self.mzn_inst["inscriptions"] = [
                {cla.int_id for cla in stu.course_id}
                for _, stu in self.students.items()
            ]
            self.mzn_inst["distribution"] = [d.nom_mzn for d in self.distributions]
            self.mzn_inst["distribution_penalty"] = [
                d.penalty if not d.required else 0 for d in self.distributions
            ]

            self.mzn_inst["distribution_classes"] = [
                [c.int_id for c in d.classes]
                + [0] * (len(self.all_classes) - len(d.classes))
                for d in self.distributions
                # if d.required
            ]

        # size_max = max([len(d.classes) for d in ins.distributions] + [0])
        # f.write(f'size_max = {size_max};\n')
        # instance['distribution_classes'] = [
        #     [d.classes[i].int_id if i < len(d.classes) else 0
        #      for i in range(size_max)] for d in ins.distributions]
        # instance['distribution_classes'] = [
        #     [ins.classes[i].int_id
        #      if ins.classes[i] in d.classes else 0
        #      for i in range(len(ins.classes))]
        #     for d in ins.distributions
        #     if d.required]
        self.mzn_inst["weight"] = [
            self.optimization.time,
            self.optimization.room,
            self.optimization.student,
            self.optimization.distribution,
        ]


def get_instance(
    xmlfile: str = "datasetITC2019/muni-fsps-spr17c.xml", light=False
) -> Instance:
    """Récupération de l'instance du fichier donné en paramètre

    Args:
        xmlfile (str, optional): nom du fichier.
        Defaults to "datasetITC2019/muni-fsps-spr17c.xml".
        light (bool, optional): True si sectioning avant la recherche.
        Defaults to False.

    Returns:
        Instance: instance
    """
    return Instance(**parse_file_object(xmlfile), light=light)
