"""
Gestion de la répartition des étudiants dans les groupes

Beaucoup des fonctions ici ne fonctionnaient pas
(du point de vue de la résolution du problème)

De plus, les librairies bottleneck et matplotlib ne
fonctionnent pas avec Pypy (permet une exécution plus rapide)
d'où les parties du code commentés

Il y a un gros tri à faire dans ce fichier...

"""

import datetime

# import itertools
# import math
import random
import time

# import warnings
# from copy import copy
from os import system
from typing import List, Tuple
from xml.etree.ElementTree import Element, parse, SubElement, tostring
from minizinc import Instance as Instance_mzn, Model, Solver
from edt.instance import Instance
from edt.instance_objects import Student, StudentGroup  # , Course
from edt.tools import chunks, pgcdn  # , save_images


# import bottleneck as bn
# import matplotlib
# import matplotlib.pyplot as plt
# import numpy as np
# import seaborn as sns

# sns.set()


def make_group_from_courses(ins: Instance, make_graph: bool = False) -> None:
    """Création des groupes à partir des cours

    Args:
        ins (Instance): instance à traiter
        make_graph (bool, optional): génération des graphes bipartis. Defaults to False.
    """
    if not ins.unique_group:
        ins.generate_unique_group()
    # Affichage des graphes bipartie si demandé
    if make_graph:
        ins.make_bipartite_graph()
    # Création des groupes bloc par bloc
    for g in ins.unique_group:
        print("*" * 50)
        print("len(g)", len(g))
        # Tri des étudiants uniques en fonction de leur nombre
        g.sort(
            key=lambda stud_group: (len(stud_group.group), len(stud_group.course_id)),
            reverse=True,
        )
        print(g)
        # Pour chaque étudiant dans chaque bloc d'étudiants uniques,
        # lui donner un numéro commun à chaque étudiant unique
        for i, us in enumerate(g):
            for s in us.group:
                s.index_group = i
        # Récupération de l'ensemble des étudiants
        students = [st for s in g for st in s.group]
        # Donner un identifiant à chaque étudiant pour cette partie
        # de la recherche
        for i, s in enumerate(students):
            s.id_for_search = i
        courses = list(set(c for s in g for c in s.course_id))
        # Placement des étudiants dans les structures des cours auquels ils
        # sont inscrits
        for c in courses:
            c.new_ls()
        # ins.rencontres = np.zeros((len(students), len(students)), float)
        # nb_stud = int(len(students) / 2)
        # ins.rencontres_score = np.zeros(len(students))
        # get_score_rencontres(ins, courses, nb_stud,
        #                      to_move=None,
        #                      plot=True,
        #                      save=True, turn=0)


def make_group(ins: Instance) -> None:
    """Création des groupes par la ppc

    Args:
        ins (Instance): instance à traiter
    """

    edt_model = Model("mzn_files/make_groups.mzn")
    gecode = Solver.lookup("gecode")
    instance = Instance_mzn(gecode, edt_model)
    with open(f"mzn_files/dzn/make_group_{ins.problem.name}.dzn", "w") as f:
        f.write(f"nr_courses = {len(ins.courses)};\n")
        instance["nr_courses"] = len(ins.courses)
        f.write(f"nr_configs = {len(ins.all_configs)};\n")
        instance["nr_configs"] = len(ins.all_configs)
        f.write(f"nr_subparts = {len(ins.all_subparts)};\n")
        instance["nr_subparts"] = len(ins.all_subparts)
        f.write(f"nr_classes = {len(ins.all_classes)};\n")
        instance["nr_classes"] = len(ins.all_classes)
        f.write(f"nr_students = {len(ins.all_students)};\n")
        instance["nr_students"] = len(ins.all_students)
        configs = [{conf.int_id for _, conf in c} for _, c in ins.courses.items()]
        f.write(f"configs = " f"{configs};\n")
        instance["configs"] = configs
        subparts = [
            {sub.int_id for _, sub in conf}
            for _, c in ins.courses.items()
            for _, conf in c
        ]
        f.write(f"subparts = {subparts};\n")
        instance["subparts"] = [
            {sub.int_id for _, sub in conf}
            for _, c in ins.courses.items()
            for _, conf in c
        ]
        classes = [
            {cla.int_id for _, cla in sub}
            for _, c in ins.courses.items()
            for _, conf in c
            for _, sub in conf
        ]
        f.write(f"classes = {classes};\n")
        instance["classes"] = classes
        parent = [cla.parent.int_id if cla.parent else 0 for cla in ins.all_classes]
        f.write(f"parent = {parent};\n")
        instance["parent"] = parent
        f.write(f"limit = {[cla.limit for cla in ins.all_classes]};\n")
        instance["limit"] = [cla.limit for cla in ins.all_classes]
        inscriptions = [
            {cla.int_id for cla in stu.course_id} for _, stu in ins.students.items()
        ]
        f.write(f"inscriptions = {inscriptions};\n")
        instance["inscriptions"] = inscriptions
    print(datetime.datetime.now())
    start_time = time.time()
    solutions = instance.solve()  # timedelta(minutes=5)
    print("--- %s seconds ---" % (time.time() - start_time))
    for x in solutions["x_class"]:
        print(x)
    ins.xclass = solutions["x_class"]
    print(solutions["x_headcount"])
    for i, ic in enumerate(solutions["x_class"]):
        ins.all_students[i].classes = []
        for c in ic:
            if c != 0:
                ins.all_classes[c - 1].add_student(ins.all_students[i])


def generate_meta_from_unique_group(
    ins: Instance, group: List[StudentGroup]
) -> Tuple[List[StudentGroup], int]:
    """Génération des groupes a partir des uniques students

    Args:
        ins (Instance): instance à traiter
        group (List[StudentGroup]): liste des uniques students

    Returns:
        Tuple[List[StudentGroup], int]: résultat du sectionnement
    """
    diff_len = [len(stu.group) for stu in group]
    print(diff_len)
    diff_len.extend(
        [
            t
            for u in group
            for c in u.course_id
            for t in ins.courses[c.id].struct_group_min
        ]
    )
    print(diff_len)
    pgcd_n = pgcdn(diff_len)
    meta_students = []
    for u in group:
        students_to_split = u.group[:]
        for lst_stu in chunks(students_to_split, pgcd_n):
            meta_students.append(StudentGroup(lst_stu[0]))
            if pgcd_n > 1:
                for i in range(1, len(lst_stu)):
                    meta_students[-1].group.append(lst_stu[i])
    return meta_students, pgcd_n


def make_group_from_structs_meta(ins: Instance, make_graph: bool = False):
    """Création des groupes

    Args:
        ins (Instance): instance à traiter
        make_graph (bool, optional): affichage des graphes. Defaults to False.
    """
    # Si les étudiants ne sont pas séparés en différents blocs d'etudiants
    # uniques dont des cours sont partagés entre les étudiants d'un bloc
    # alors création des blocs
    if not ins.unique_group:
        ins.generate_unique_group()
    # Affichage des graphes bipartie si demandé
    if make_graph:
        ins.make_bipartite_graph()
    # Création des groupes bloc par bloc
    for g in ins.unique_group:
        print(len(g))
        # meta_students, pgcd = generate_meta_from_unique_group(ins, g)
        # print("%" * 20, pgcd)


def voisins_after_delete(
    student: Student, old_positions: List[Tuple[int, int]]
) -> List[List[Tuple[List[List[Student]], Student]]]:
    """Génération des voisins après la suppression de l'étudiant student
    dans les différentes structures des cours qu'il suit.

    Args:
        student (Student): Étudiant à déplacer dans les différentes structures
        old_positions (List[Tuple[int, int]]): liste les anciennes positions de
        l'étudiant dans les différents cours

    Returns:
        List[List[Tuple[List[List[Student]], Student]]]: voisins
    """
    voisins = []
    # Pour chaque cours auquel assiste l'étudiant
    for index_cours, cours in enumerate(student.course_id):
        # On ajoute une possibilité de voisins
        voisins.append([])
        if len(cours.struct_registered) > 1 and random.random() < 0.3:
            new_struct = [cop[::] for cop in cours.struct_registered]
            old_position = old_positions[index_cours]
            new_struct[old_position[0]][old_position[1]] = student
            voisins[index_cours].append((new_struct, None))
            continue
        # Pour chacune des stuctures possibles
        for index_struct, struct in enumerate(cours.struct_registered):
            already_swap_with_group = []
            # Si la structure courante correspond à l'ancienne structure
            # de l'étudiant
            if old_positions[index_cours][0] == index_struct:
                # Copie de la structure
                new_struct = [cop[::] for cop in cours.struct_registered]
                # On ajoute l'étudiant dans la première place vide
                index = new_struct[index_struct].index(None)
                new_struct[index_struct][index] = student
                # On ajoute l'état aux voisins
                voisins[index_cours].append((new_struct, None))
            else:
                # Sinon, l'étudiant n'était pas dans cette structure
                already_none = False
                # Pour chacune des positions dans la structure
                for index_position, element in enumerate(struct):
                    # Si la position est None
                    if element is None:
                        # Si une position None n'a pas déjà été attribuée
                        # dans cette structutre
                        if not already_none:
                            # Ajout de l'étudiant à cette position
                            # Copie de la structure
                            new_struct = [cop[::] for cop in cours.struct_registered]
                            new_struct[index_struct][index_position] = student
                            # On ajoute l'état aux voisins
                            voisins[index_cours].append((new_struct, None))
                            already_none = True
                    # Sinon, si la position contient un étudiant du même
                    # groupe
                    elif element.index_group != student.index_group:
                        if element.index_group in already_swap_with_group:
                            if already_swap_with_group.count(element.index_group) > 2:
                                if random.random() < 0.5:
                                    continue
                        else:
                            already_swap_with_group.append(element.index_group)
                        # Ajout de l'étudiant à cette position
                        # Copie de la structure
                        new_struct = [cop[::] for cop in cours.struct_registered]
                        # Récupération de l'étudiant placé à cette position
                        old_stud = element
                        # Place l'étudiant à sa nouvelle place
                        new_struct[index_struct][index_position] = student
                        # Place l'étudiant à l'ancienne place
                        old_place = old_positions[index_cours]
                        new_struct[old_place[0]][old_place[1]] = old_stud
                        # Ajout de la structure aux voisins
                        voisins[index_cours].append((new_struct, old_stud))
    return voisins


def save_inscriptions_xml(ins: Instance) -> None:
    """Enregistrement des groupes au format XML

    Args:
        ins (Instance): instance à traiter
    """
    inscriptions = Element("inscriptions", {"name": ins.problem.name})
    for c in ins.all_classes:
        attribs = {"id": str(c.id)}
        child = SubElement(inscriptions, "class", attribs)
        for s in c.registered:
            SubElement(child, "student", {"id": s.id})
    ins_file = f"ressources/inscriptions/" f"inscription_{ins.problem.name}.xml"
    with open(ins_file, "w") as xml_f:
        xml_f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        xml_f.write(
            "<!DOCTYPE inscription PUBLIC "
            '"-//UA 2020//DTD Solution Format/EN" '
            '"edt-format-solution.dtd">'
        )
        xml_f.write(tostring(inscriptions).decode("utf8"))
    system(f"cat {ins_file} |" f" xmlstarlet format --indent-tab > {ins_file}_tmp")
    system(f"mv {ins_file}_tmp {ins_file}")


def load_inscriptions_xml(ins: Instance, inscriptions_file: str) -> None:
    """Chargement des inscriptions

    Args:
        ins (Instance): instance à traiter
        inscriptions_file (str): fichier XML des inscriptions
    """
    tree = parse(inscriptions_file)
    inscription = tree.getroot()

    if inscription.attrib["name"] != ins.problem.name:
        print("Inscription file and instance have different names")

    for child in inscription:
        if child.tag == "class":
            class_name = child.attrib["id"]
            studs = [
                ins.students[stud.attrib["id"]]
                for stud in child
                if stud.tag == "student"
            ]
            real_class = [c for c in ins.all_classes if c.id == class_name][0]
            real_class.remove_all_students()
            for s in studs:
                real_class.add_student(s)
    ins.generate_xclass()


# def make_group_from_structs(ins: Instance, make_graph: bool = False) -> None:
#     # Si les étudiants ne sont pas séparés en différents blocs d'etudiants
#     # uniques dont des cours sont partagés entre les étudiants d'un bloc
#     # alors création des blocs
#     if not ins.unique_group:
#         ins.generate_unique_group()
#     # Affichage des graphes bipartie si demandé
#     if make_graph:
#         ins.make_bipartite_graph()
#     # Création des groupes bloc par bloc
#     for numero, g in enumerate(ins.unique_group):
#         print('*' * 50)
#         print('len(g)', len(g))
#         # Tri des étudiants uniques en fonction de leur nombre
#         g.sort(key=lambda stud_group: (
#             len(stud_group.group), len(stud_group.course_id)),
#                reverse=True)
#         # Pour chaque étudiant dans chaque bloc d'étudiants uniques,
#         # lui donner un numéro commun à chaque étudiant unique
#         for i, us in enumerate(g):
#             for s in us.group:
#                 s.index_group = i
#         # Récupération de l'ensemble des étudiants
#         students = [st for s in g for st in s.group]
#         # Donner un identifiant à chaque étudiant pour cette partie
#         # de la recherche
#         print(g)
#         for i, s in enumerate(students):
#             s.id_for_search = i
#         # Récupération de l'ensemble des cours
#         courses = list(set([c for s in g for c in s.course_id]))
#         # Placement des étudiants dans les structures des cours auquels ils
#         # sont inscrits
#         for c in courses:
#             c.place_students()
#         # Si chaque cours du bloc ne possède qu'une seule structure,
#         # le placement des étudiants pour cette structure est terminé
#         if all([len(c.structures) == 1 for c in g[0].course_id]):
#             continue
#         # if numero == 4 or numero == 5:
#         #     continue
#         last_picked = []
#         scores = []
#         turn = -1
#         ins.rencontres = np.zeros((len(students), len(students)), float)
#         nb_stud = int(len(students) / 2)
#         ins.rencontres_score = np.zeros(len(students))
#         to_move = None
#         while turn < int(len(students) * 100):
#             turn += 1
#             print('turn', turn)
#             # Récupération de la matrice des rencontres entre étudiants
#             scores.append(
#                 get_score_rencontres(ins, courses, nb_stud,
#                                      to_move=to_move,
#                                      plot=True,
#                                      save=True, turn=turn))
#             for c in courses:
#                 for s in c.struct_registered:
#                     random.shuffle(s)
#             # # Récupération des coordonées dans la matrice des rencontres
#             # # où les rencontres sont différentes de Nan
#             # possible_moves = np.argwhere(
#             #     np.logical_not(np.isnan(rencontres)))
#             # Récupération de la liste des étudiants que l'on peut utiliser
#             # pour la recherche excepté ceux présents dans la liste tabou
#             # poss_students = list(set(
#             #     [s for s in students if s.id not in last_picked[-10:]]))
#             # students[p[0]]
#             # for p in possible_moves
#             # if students[p[0]].id not in last_picked[-10:]]))
#             # Choix d'un des étudiants au hasard
#             # if turn < len(students):
#             #     to_move = students[turn]
#             # else:
#             # poss_stud = [s for s in students
#             #              if
#             #              s.id not in last_picked[-int(nb_stud):]]
#             # weights = [-int(self.rencontres_score[s.id_for_search] /
#             #                 len(s.course_id) * 10)
#             #            for s in poss_stud]
#             # print(weights)
#             # to_move = random.choices(population=poss_stud,
#             #                          weights=weights)[0]
#             to_move = random.choice(students)
#             # Ajout de l'étudiant à la liste tabou
#             last_picked.append(to_move.id)
#             print(to_move, to_move.id_for_search)
#             # Récupération des anciennes positions de l'étudiant dans les
#             # cours et suppression de celui ci dans celles ci
#             old_positions = [c.remove_stud_from_struct_registered(to_move)
#                              # if random.random() < 0.3 else
#                              # -1
#                              for c in to_move.course_id]
#             t = time.time()
#             # Création des voisins possibles
#             voisins = voisins_after_delete(to_move, old_positions)
#             print(f'nb voisins : {[len(v) for v in voisins]} '
#                   f'({math.prod([len(v) for v in voisins])} voisins)')
#             # Créations des possibilités en fonction des voisins générés
#             ranges = [range(len(liste)) for liste in voisins]
#             possibilities = itertools.product(*ranges)
#             scores_possibility = []
#             # Pour chaque possibilité on place la configuration de
#             # structure puis on calcule le score
#             for possib in possibilities:
#                 modified = [to_move]
#                 for index_cours, index_rang_v in enumerate(possib):
#                     modified.append(voisins[index_cours][index_rang_v][1])
#                     to_move.course_id[index_cours].struct_registered = \
#                         voisins[index_cours][index_rang_v][0]
#                 score = get_score_rencontres_run(ins, students,
#                                                  nb_stud,
#                                                  modified=modified)
#                 scores_possibility.append((score, possib))
#             scores_possibility.sort(key=lambda x: x[0], reverse=True)
#             # On récupère la possibilité avec le score le plus faible
#             # score, possibility = min(scores_possibility,
#             #                          key=lambda x: x[0])
#             if len(scores_possibility) == 1:
#                 score, possibility = scores_possibility[0]
#             else:
#                 score, possibility = scores_possibility[
#                     random.choices(population=[0, 1],
#                                    weights=[100, 10])[0]]
#             # On place cette possibilité comme solution courante
#             for index_cours, index_rang_v in enumerate(possibility):
#                 to_move.course_id[index_cours].struct_registered = \
#                     voisins[index_cours][index_rang_v][0]
#             print('temps :', time.time() - t)
#             print('score :', score)
#             # for c in to_move.course_id:
#             #     if len(c.struct_registered) != 1:
#             #         scores_renco = []
#             #         voisins = c.voisins_stud(to_move)
#             #         for v in voisins:
#             #             c.struct_registered = v
#             #             rencon = self.get_rencontres(students,courses,g)
#             #             score = self.score_rencontres(rencon)
#             #             # self.plot_rencontres(renco[-1])
#             #             scores_renco.append((score, v))
#             #         min_score = min(scores_renco, key=lambda x: x[0])
#             #         c.struct_registered = min_score[1]
#             #         print('score', min_score[0])
#             # print('=' * 50)
#             # for c in courses:
#             #     print(
#             #         [[e.index_group if e is not None else 'None' for e in
#             #           l]
#             #          for l in c.struct_registered])
#             # rencontres = self.get_rencontres(students, courses, g)
#             # score = self.score_rencontres(rencontres)
#             # self.plot_rencontres(rencontres, save=True, i=turn,
#             #                      score=score)
#             # print('=' * 50)
#             # for c in courses:
#             #     print(f'Course {c.id}')
#             #     print([[e.index_group
#             #             if e is not None else 'None'
#             #             for e in struct]
#             #            for struct in c.struct_registered])
#             #     print([[e.id
#             #             if e is not None else 'None'
#             #             for e in struct]
#             #            for struct in c.struct_registered])
#             # to_move = random.choice(poss_students)
#             # (voir pour optimiser ce choix)
#
#         t = np.arange(0, turn, 1)
#         fig, ax = plt.subplots()
#         ax.plot(t, scores)
#
#         ax.set(xlabel='turn', ylabel='score',
#                title='Student Sectionning score')
#         ax.grid()
#
#         fig.savefig("sss.png")
#         plt.show()
#         save_images(numero)


# def get_score_rencontres_run(ins: Instance, students: List[Student],
#                              nb_stud: int,
#                              modified=None):
#     # current_cmap = matplotlib.cm.get_cmap()
#     # current_cmap.set_bad(color='white')
#     # plt.imshow(self.rencontres)
#     # plt.colorbar()
#     # plt.title(f'before modification')
#     # plt.show()
#     copies = []
#     for e in modified:
#         if e is not None:
#             copies.append(copy(ins.rencontres[e.id_for_search, :]))
#     for e in modified:
#         if e is not None:
#             for i in range(len(students)):
#                 ins.rencontres[e.id_for_search, i] = 0
#                 ins.rencontres[i, e.id_for_search] = 0
#     # current_cmap = matplotlib.cm.get_cmap()
#     # current_cmap.set_bad(color='white')
#     # plt.imshow(self.rencontres)
#     # plt.colorbar()
#     # plt.title(f'after None')
#     # plt.show()
#     lines = []
#     for e in modified:
#         line = np.zeros(len(students))
#         if e is not None:
#             for c in e.course_id:
#                 struct = c.get_struct(e)
#                 for stud in struct:
#                     if stud is not None:
#                         if stud.id != e.id:
#                             if stud.index_group == e.index_group:
#                                 line[stud.id_for_search] += 1
#                             else:
#                                 line[stud.id_for_search] += 1
#                         else:
#                             line[stud.id_for_search] = None
#         lines.append(line)
#     for i, e in enumerate(modified):
#         if e is not None:
#             ins.rencontres[e.id_for_search, :] = lines[i]
#             ins.rencontres[:, e.id_for_search] = lines[i]
#     for i in range(len(ins.rencontres)):
#         ins.rencontres_score[i] = bn.nanmean(
#             bn.partition(- ins.rencontres[i], nb_stud)[:nb_stud])
#         # / (len(students[i].course_id))
#     score = - bn.nanmean(ins.rencontres_score)
#     # current_cmap = matplotlib.cm.get_cmap()
#     # current_cmap.set_bad(color='white')
#     # plt.imshow(self.rencontres)
#     # plt.colorbar()
#     # plt.title(f'before back')
#     # plt.show()
#     j = 0
#     for e in copies:
#         while modified[j] is None:
#             j += 1
#         ins.rencontres[modified[j].id_for_search, :] = e
#         ins.rencontres[:, modified[j].id_for_search] = e
#         j += 1
#     # current_cmap = matplotlib.cm.get_cmap()
#     # current_cmap.set_bad(color='white')
#     # plt.imshow(self.rencontres)
#     # plt.colorbar()
#     # plt.title(f'after back')
#     # plt.show()
#     return score


# def get_score_rencontres(ins, courses: List[Course], nb_stud: int,
#                          to_move: Student = None,
#                          plot: bool = False, save: bool = False,
#                          turn: int = None):
#     # nonzero = np.count_nonzero(rencontres, axis=1)
#     # rencontres[rencontres == 0] = np.nan
#     # with warnings.catch_warnings():
#     #     warnings.simplefilter("ignore", category=RuntimeWarning)
#     #     means = np.nanmean(rencontres[:, 1:], axis=1)
#     # on veut diminuer le nombre de nonzero et augmenter les moyennes
#     # maxp = len(rencontres)
#     # mean_nonzero = np.mean(nonzero)
#     # mean_means = np.nanmean(means)
#     # print('maxp', maxp)
#     # print(nonzero)
#     # print('mean_nonzero', mean_nonzero)
#     # print(means)
#     # print('mean_means', mean_means)
#     # n = int(len(rencontres)*0.3)
#     ins.rencontres.fill(0)
#     for c in courses:
#         for struct in c.struct_registered:
#             for s1 in struct:
#                 for s2 in struct:
#                     if s1 is not None and s2 is not None:
#                         if s1.id != s2.id:
#                             if s1.index_group == s2.index_group:
#                                 ins.rencontres[
#                                     s1.id_for_search,
#                                     s2.id_for_search] += 1
#                             else:
#                                 ins.rencontres[
#                                     s1.id_for_search,
#                                     s2.id_for_search] += 1
#                         else:
#                             ins.rencontres[
#                                 s1.id_for_search, s2.id_for_search] = None
#     for i in range(len(ins.rencontres)):
#         ins.rencontres_score[i] = bn.nanmean(
#             bn.partition(- ins.rencontres[i], nb_stud)[:nb_stud])
#         # / (len(students[i].course_id))
#     score = - bn.nanmean(ins.rencontres_score)
#     if plot:
#         # noinspection PyUnresolvedReferences
#         current_cmap = matplotlib.cm.get_cmap()
#         current_cmap.set_bad(color='white')
#         plt.imshow(ins.rencontres)
#         if to_move is not None:
#             plt.axvline(to_move.id_for_search, color='red')
#             plt.axhline(to_move.id_for_search, color='red')
#         plt.colorbar()
#         if save:
#             plt.title(f'turn {turn} - score {score}')
#             plt.savefig(f'images/plot_{turn:03}.png')
#         plt.show()
#     return score


# def plot_rencontres(ins: Instance, title=""):
#     if not ins.unique_group:
#         ins.generate_unique_group()
#     # ins.make_bipartite_graph()
#     for numero, g in enumerate(ins.unique_group):
#         g.sort(key=lambda stud_group: (
#             len(stud_group.group), len(stud_group.course_id)), reverse=True)
#         for i, us in enumerate(g):
#             for s in us.group:
#                 s.index_group = i
#         students = [st for s in g for st in s.group]
#         for i, s in enumerate(students):
#             s.id_for_search = i
#         courses = list(set([c for s in g for c in s.course_id]))
#         rencontres = np.zeros((len(students), len(students)), int)
#         for c in courses:
#             classes = [cl
#                        for _, conf in c.configs.items()
#                        for _, sub in conf.subparts.items()
#                        for _, cl in sub.classes.items()]
#             for cl in classes:
#                 for s1 in cl.registered:
#                     for s2 in cl.registered:
#                         if s1 is not None and s2 is not None:
#                             rencontres[s1.id_for_search,
#                             s2.id_for_search] += 1
#         # noinspection PyUnresolvedReferences
#         current_cmap = matplotlib.cm.get_cmap()
#         current_cmap.set_bad(color='white')
#         plt.imshow(rencontres)
#         # sns.heatmap(rencontres, annot=True, cmap="YlGnBu")
#         sns.heatmap(rencontres, cmap="YlGnBu")
#         # plt.colorbar()
#         plt.title(title)
#         plt.savefig(f'rencontres_{title}_{ins.problem.name}_{numero}.png')
#         plt.show()


# make group from structs
# # courses = g[0].course_id
# # print([c.struct_group_min for c in courses])
# # if len(g) == 1:
# #     courses = g[0].course_id
# #     if all([len(c.struct_group_min) == 1 for c in courses]):
# #         for c in courses:
# #             for stud in g[0].group:
# #                 c.add_student_to_first_struct(stud)
# #         print('OK')
# #     else:
# #         print('la')
# #         # taille_min = min([])
# #     # for stud in g[0].group:
# #     #     for c in stud.course_id:
# #     #
# #     #         c.add_student_to_first_struct(stud)
# # else:
# courses = list(set([c for gn in g for c in gn.course_id]))
# for i, c in enumerate(courses):
#     c.id_for_search = i
# size_struct_courses = [c.struct_group_min for c in courses]
# places = [[[-1] * t for t in stru]
#           for stru in size_struct_courses]
# etudiants_par_cours = [[] for _ in courses]
# for i, stud_gr in enumerate(g):
#     for st in stud_gr.group:
#         st.id_for_search = i
# for c in courses:
#     for st in c.registered:
#         etudiants_par_cours[c.id_for_search].append(
#             st.id_for_search)
# for cours in etudiants_par_cours:
#     cours.sort()
# for i in range(len(courses)):
#     print('/' * 10)
#     print(etudiants_par_cours[i])
#     print(places[i])
#     nb_etudiants = len(etudiants_par_cours[i])
#     nb_structs = len(places[i])
#     from math import ceil
#     divide_by = ceil(nb_etudiants / nb_structs)
#     print('nb_etudiants', nb_etudiants)
#     print('nb_structs', nb_structs)
#     print('divide_by', divide_by)
#     for sub_list in chunks(etudiants_par_cours[i], divide_by):
#         print(sub_list)
#     # pour chaque cours
#     # ajouter
# # liste_d_etudiants_par_cours
# # for unique_stud in g:
# #     print(len(unique_stud.group))
# #     for c in unique_stud.course_id:
# #         print(c.struct_group_min)
# #         print(c.struct_registered)
# #         added = False
# #         for i in range(len(c.struct_group_min)):
# #             if c.struct_group_min[i] == len(
# #                     unique_stud.group) and not added:
# #                 print('\t\t\t\t\t\t\tparfait')
# #                 for stud in unique_stud.group:
# #                     added = True
# #                     c.add_student_to_struct(stud, i)
# #                 print(c.struct_registered)

# # check de la présence de tous les étudiants dans les groupes
# problem_registered = []
# for c in courses:
#     for etud in c.registered:
#         ok = False
#         for struc in c.struct_registered:
#             if etud.id in [e.id
#                            for e in struc
#                            if e is not None]:
#                 ok = True
#         if not ok:
#             problem_registered.append(('course ' + c.id, etud))
# print('problem_registered')
# print(problem_registered)
# # get score old same attendees

# problems_students = []
# for _, student in self.students.items():
#     for i in range(len(student.classes)):
#         for j in range(i + 1, len(student.classes)):
#             ci = student.classes[i]
#             cj = student.classes[j]
#             if ci.same_attendees(cj):
#                 problems_students.append((ci, cj))
#                 ci.student_clashes += 1
#                 cj.student_clashes += 1
# def generate_meta_students(self):
#     for _, stud in self.students.items():
#         added = False
#         it_g = iter(self.group)
#         try:
#             g = next(it_g)
#         except StopIteration:
#             g = None
#         while not added and g:
#             if any([set(stud.course_id).intersection(gc.course_id)
#             != set()
#                     for gc in g]):
#                 g.append(stud)
#                 added = True
#             try:
#                 g = next(it_g)
#             except StopIteration:
#                 g = None
#         if not added:
#             self.group.append([stud])
#     for g in self.group:
#         unique_students = set()
#         for s in g:
#             if s in unique_students:
#                 for us in unique_students:
#                     if s == us:
#                         us.group.append(s)
#             else:
#                 unique_students.add(StudentGroup(s))
#         self.unique_group.append(unique_students)
#         diff_len = [len(stu.group) for stu in unique_students]
#         diff_len.extend([t for u in g for c in u.course_id for t in
#                          self.courses[c.id].struct_group_min])
#         pgcd_n = pgcdn(diff_len)
#         for u in unique_students:
#             students_to_split = u.group[:]
#             for lst_stu in chunks(students_to_split, pgcd_n):
#                 self.meta_students.append(StudentGroup(lst_stu[0]))
#                 if pgcd_n > 1:
#                     for i in range(1, len(lst_stu)):
#                         self.meta_students[-1].group.append(lst_stu[i])
# problem_same_attends = []
# for classes, nb in self.same_attends.items():
#     for i in range(len(classes)):
#         for j in range(i + 1, len(classes)):
#             ci = classes[i]
#             cj = classes[j]
#             if not ((ci.time.end +
#                      ci.room[0].travel[cj.room[0].int_id - 1][1]
#                      <= cj.time.start)
#                     or
#                     (cj.time.end +
#                      cj.room[0].travel[ci.room[0].int_id - 1][1]
#                      <= ci.time.start)
#                     or
#                     ((len(
#                         set(ci.time.days).intersection(
#                             cj.time.days)) == 0)
#                      or
#                      (len(set(ci.time.weeks).intersection(
#                          cj.time.weeks)) == 0))):
#                 ci.student_clashes += nb
#                 cj.student_clashes += nb
#                 problem_same_attends.append((ci, cj))
