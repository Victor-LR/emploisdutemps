"""
Ensemble des objets de base du projet
"""
import itertools
import pprint
import random
import re
from typing import List, Mapping, Optional, Tuple, Generator
from collections import Counter

import numpy as np
from termcolor import colored

from edt.tools import pgcdn


class Student:
    """Représentation des étudiants"""

    counter = 0

    def __init__(self, id_, course_id):
        self.id: str = id_
        self.course_id: List[Course] = course_id
        self.course_id.sort(key=lambda c: c.int_id)
        self.classes: List[Class] = []
        Student.counter += 1
        self.int_id: int = Student.counter
        self.id_for_search: int = 0
        self.index_group: int = 0

    def __repr__(self):
        # return f'Student {self.id}/{self.id_for_search} ({self.index_group})'
        return (
            f"Student {self.id} ({self.int_id}) : " f"{[c.id for c in self.course_id]}"
        )

    def __eq__(self, other):
        if not hasattr(other, "course_id"):
            return False
        return set(self.course_id) == set(other.course_id)

    def __hash__(self):
        return hash(tuple(self.course_id))

    def include(self, other) -> int:
        """Renvoie 1 si les cours de l'étudiants sont compris dans ceux de l'autre,
        -1 si c'est l'inverse, 0 sinon

        Args:
            other (any): l'autre étudiant

        Returns:
            int: résultat
        """
        sel = set(self.course_id)
        oth = set(other.course_id)
        inter_other = np.intersect1d(sel, oth)
        if inter_other == sel:
            # self is included in the other student,
            # he must become part of his group
            return 1
        if inter_other == oth:
            # other is included in self,
            # self become the biggest part of the group
            return -1
        return 0


class StudentGroup:
    """Représentation des groupes d'étudiants"""

    def __init__(self, student: Student):
        self.course_id = student.course_id
        self.group = [student]

    def __repr__(self):
        return f"{len(self.group)} : {[c.id for c in self.course_id]}"

    def __eq__(self, other):
        return set(self.course_id) == set(other.course_id)

    def __hash__(self):
        return hash(tuple(self.course_id))


class Room:
    """Représentation des salles de classe"""

    counter = 0

    def __init__(self, id_, capacity, unavailable, travel):
        self.id = id_
        self.capacity = capacity
        self.unavailable = unavailable
        self.travel = travel
        Room.counter += 1
        self.int_id = Room.counter
        self.users: List[Class] = []

    def __repr__(self):
        s = colored(
            f"Room {self.id} ({self.int_id})," f"capacity : {self.capacity}", "red"
        )
        # for u in self.unavailable:
        #     s += '\n\t' + repr(u)
        # for t in self.travel:
        #     s += f'\n\tTravel to {t[0]} : {t[1]}'
        return s

    def __eq__(self, other):
        return self.int_id == other.int_id


class Time:
    """Représentation des créneaux"""

    counter = 0

    def __init__(self, days, start, length, weeks, penalty=-1):
        self.days_str = days
        self.weeks_str = weeks
        self.days = [m.start() for m in re.finditer("1", days)]
        self.start = start
        self.length = length
        self.end = start + length
        self.weeks_bool = int(weeks, 2)
        self.days_bool = int(days, 2)
        self.weeks = [m.start() for m in re.finditer("1", weeks)]
        self.penalty = penalty
        Time.counter += 1
        self.int_id = Time.counter
        self.id_for_search = self.int_id

    def get_num_creneau(self, min_timeslot: int, max_timeslot: int) -> int:
        """Récupère le créneau

        Args:
            min_timeslot (int): temps le plus tôt
            max_timeslot (int): temps le plus tard

        Returns:
            int: créneau
        """
        return (
            (self.start - min_timeslot)
            + (max_timeslot - min_timeslot) * self.days[0]
            + self.weeks[0] * (max_timeslot - min_timeslot) * 7
        )

    def __repr__(self):
        s = (
            f"Days {self.days_str}, "
            f"start {self.start}, "
            f"length {self.length}, "
            f"weeks {self.weeks_str}"
        )
        if self.penalty != -1:
            s += f" penalty {self.penalty}"
        return colored(s, "blue")

    def __eq__(self, other):
        return (
            self.days_bool == other.days_bool
            and self.start == other.start
            and self.length == other.length
            and self.weeks_bool == other.weeks_bool
        )

    def overlap(self, time2):
        """Renvoie Vrai si les deux créneaux se chevauchent

        Args:
            time2 (any): l'autre temps

        Returns:
            bool: Vrai si les temps se chevauchent
        """
        return (
            time2.weeks_bool & self.weeks_bool
            and time2.days_bool & self.days_bool
            and (
                (self.start < time2.end and time2.start < self.end)
                or (time2.start < self.end and self.start < time2.end)
            )
        )

    def same_time(self, other: any) -> bool:
        """Renvoie vrai si les deux temps se déroulent en même temps

        Args:
            other (any): un autre créneau

        Returns:
            bool: Vrai si les créneaux se déroulent en même temps
        """
        if other.weeks_bool & self.weeks_bool:
            if other.days_bool & self.days_bool:
                return not (other.end <= self.start or self.end <= other.start)
        return False

    def before(self, other) -> bool:
        """Renvoie Vrai si le créneau se déroule avant l'autre

        Args:
            other (any): l'autre créneau

        Returns:
            bool: Vrai si l'autre créneau se déroule aprés celui ci
        """
        if other.weeks[0] < self.weeks[0]:
            return False
        if other.weeks[0] > self.weeks[0]:
            return True
        if other.days[0] < self.days[0]:
            return False
        if other.days[0] > self.days[0]:
            return True
        return self.start + self.length < other.start


class Class:
    """Représentation des séances"""

    counter = 0

    def __init__(
        self, id_, limit, parent, classrooms, times, rooms: bool, subpart=None
    ):
        """

        @param id_:
        @type id_:
        @param limit:
        @type limit:
        @param parent:
        @type parent:
        @param classrooms:
        @type classrooms:
        @param times:
        @type times:
        @param rooms: False if the course is in a virtual room
        @type rooms: bool
        @param subpart:
        @type subpart:
        """
        self.id: str = id_
        self.limit: int = limit
        self.parent: Class = parent
        self.classrooms: List[Tuple[Room, int]] = classrooms
        times.sort(key=lambda x: x.penalty, reverse=False)
        self.times: List[Time] = times
        self.rooms: bool = rooms
        self.subpart: Subpart = subpart
        self.registered: List[Student] = []
        self.nb_registered = 0
        Class.counter += 1
        self.int_id = Class.counter
        self.id_for_search = 0
        self.time: Optional[Time] = None
        self._room: Optional[Tuple[Room, int]] = None
        self.student_clashes: int = 0
        self.room_clashes: int = 0
        self.hard_distribution_clashes = 0
        self.soft_distribution_clashes = 0
        self.pseudo_same_attendees_clashes = 0
        self.times_for_search = self.times
        self.rooms_for_search = self.classrooms
        self.in_distri = []
        self.in_peusdo_same_attendees = []

    def __repr__(self):
        s = f"Class {self.id} ({self.int_id}) limit : {self.limit}"
        if self.parent:
            s += f" parent {self.parent}"
        if not self.rooms:
            s += " no room"
        if self.subpart:
            s += f" subpart id : {self.subpart.id}"
        if len(self.registered) != 0:
            s += f" registered : {[s.id for s in self.registered]}"
        # for c in self.classrooms:
        #     s += '\n\t\t\t\t' + repr(c[0]) + ' penalty : ' + c[1]
        # for t in self.times:
        #     s += '\n\t\t\t\t' + repr(t)
        return s

    @property
    def room(self) -> Optional[Tuple[Room, int]]:
        """récupération de la salle de classe

        Returns:
            Optional[Tuple[Room, int]]: la salle de classe de la séance
        """
        return self._room

    @room.setter
    def room(self, r: Optional[Tuple[Room, int]]):
        """Modification de l'affectation de la salle

        Args:
            r (Optional[Tuple[Room, int]]): Salle à placer
        """
        if r != self._room:
            if self._room is not None:
                self._room[0].users.remove(self)
            if r is not None:
                r[0].users.append(self)
            self._room = r

    def reset_counter(self):
        """RAZ des compteurs de contraintes"""
        self.student_clashes = 0
        self.room_clashes = 0
        self.hard_distribution_clashes = 0
        self.soft_distribution_clashes = 0
        self.pseudo_same_attendees_clashes = 0

    def increment_soft(self, val: int):
        """Incrémentation du compteur de contrainte souple

        Args:
            val (int): poids de la contrainte
        """
        self.soft_distribution_clashes += val

    def increment_hard(self):
        """Incrémentation du compteur de contraite stricte"""
        self.hard_distribution_clashes += 1

    def add_student(self, student):
        """Inscription d'un étudiant

        Args:
            student ([type]): étudiant à inscrire
        """
        self.registered.append(student)
        student.classes.append(self)

    def remove_student(self, student: Student):
        """Suppression de l'étudiant donné

        Args:
            student (Student): étudiant à désinscrire
        """
        self.registered.remove(student)
        student.classes.remove(self)

    def remove_all_students(self):
        """Suppression des étudiants inscrits à cette séance"""
        while self.registered:
            stud = self.registered.pop()
            stud.classes.remove(self)

    def same_attendees(self, other) -> bool:
        """Renvoie vrai si les cours se déroulent en même temps

        Args:
            other (Class): une autre séance

        Returns:
            bool: Vrai si la contrainte SameAttendees n'est pas respectée
        """
        if other.time.weeks_bool & self.time.weeks_bool:
            if other.time.days_bool & self.time.days_bool:
                return not (
                    other.time.end + self.room[0].travel[other.room[0].int_id - 1][1]
                    <= self.time.start
                    or self.time.end + self.room[0].travel[other.room[0].int_id - 1][1]
                    <= other.time.start
                )
        return False

    def total_hard_clashes(self) -> int:
        """Renvoie le score strict

        Returns:
            int: score strict
        """
        return self.student_clashes + self.room_clashes + self.hard_distribution_clashes

    def total_soft_clashes(self) -> int:
        """Renvoie le score souple

        Returns:
            int: score souple
        """
        if self.time is not None and self.room is not None:
            return self.soft_distribution_clashes + self.time.penalty + self.room[1]
        return self.soft_distribution_clashes


class Subpart:
    """Représentation des sous parties"""

    counter = 0

    def __init__(self, id_, classes, config=None):
        self.id = id_
        self.classes = classes
        self.config = config
        Subpart.counter += 1
        self.id_for_search = 0
        self.int_id = Subpart.counter

    def __repr__(self):
        s = f"Subpart {self.id} ({self.int_id})"
        if self.config:
            s += f" config id : {self.config.id}"
        for _, c in self.classes.items():
            s += "\n\t\t\t" + repr(c)
        return s

    def __getitem__(self, item):
        return self.classes[item]

    def __iter__(self):
        for k, v in self.classes.items():
            yield k, v


class Config:
    """Représentation des configurations"""

    counter = 0

    def __init__(self, id_: str, subparts, course=None):
        self.id = id_
        self.subparts = subparts
        self.course = course
        Config.counter += 1
        self.id_for_search = 0
        self.int_id = Config.counter

    def __repr__(self) -> str:
        s = f"Config {self.id} ({self.int_id})"
        if self.course:
            s += f" course id : {self.course.id}"
        for _, sub in self.subparts.items():
            s += "\n\t\t" + repr(sub)
        return s

    def __getitem__(self, item):
        return self.subparts[item]

    def __iter__(self):
        for k, v in self.subparts.items():
            yield k, v


class Course:
    """Représentation des cours"""

    counter = 0

    def __init__(self, id_: str, configs: Mapping[str, Config], no_student: bool):
        self.id = id_
        self.configs = configs
        Course.counter += 1
        self.int_id = Course.counter
        self.id_for_search = 0
        self.structures = []
        self.struct_group_min = []
        if not no_student:
            self.get_diff_structures()
        self.registered = []
        # for struct in self.structures:
        #     size_min = struct[0].limit
        #     for cla in struct:
        #         if cla.limit < size_min:
        #             size_min = cla.limit
        #     self.struct_group_min.append(size_min)
        self.struct_registered = [
            [None] * self.struct_group_min[i] for i in range(len(self.structures))
        ]

    def __repr__(self) -> str:
        s = (
            f"Course {self.id} ({self.int_id}) : "
            f"nb_registered : {len(self.registered)}"
        )
        s += self.str_structures()
        for _, c in self.configs.items():
            s += "\n\t" + repr(c)
        return s

    def __getitem__(self, item):
        return self.configs[item]

    def __iter__(self):
        for k, v in self.configs.items():
            yield k, v

    def get_struct(self, student: Student) -> list:
        """Récupération de la structure d'un étudiant

        Args:
            student (Student): l'étudiant à chercher

        Raises:
            Exception: Exception si étudiant non trouvé

        Returns:
            list: la structure de l'étudiant
        """
        for struct in self.struct_registered:
            if student.id in [e.id for e in struct if e is not None]:
                return struct
        raise Exception(f"{student} not found in {self.struct_registered}")

    def remove_stud_from_struct_registered(self, student: Student) -> Tuple[int, int]:
        """Suppression d'un étudiant de sa structure

        Args:
            student (Student): étudiant à supprimer

        Raises:
            Exception: Exception si l'étudiant n'est pas trouvé

        Returns:
            Tuple[int, int]: ancienne position de l'étudiant
        """
        pos = -1
        struct = -1
        for i, s in enumerate(self.struct_registered):
            for j, e in enumerate(s):
                if e is not None and student.id == e.id:
                    struct = i
                    pos = j
                    self.struct_registered[struct][pos] = None
        if pos == -1 and struct == -1:
            raise Exception("problem")
        return struct, pos

    def place_students(self) -> None:
        """Placement des étudiants dans les structures aléatoirement"""
        self.registered.sort(key=lambda x: x.index_group)
        for s in self.registered:
            # i = None
            # for j in range(len(self.structures)):
            #     if i is not None:
            #         continue
            #     else:
            #         if any([a is None for a in self.struct_registered[j]]):
            #             i = j
            randchoices = [
                i
                for i in range(len(self.structures))
                if any([a is None for a in self.struct_registered[i]])
            ]
            struct = random.choice(randchoices)
            index = self.struct_registered[struct].index(None)
            self.struct_registered[struct][index] = s

    def place_registered(self) -> None:
        """Placement des étudiants dans les structures"""
        for stud, struct in zip(self.struct_registered, self.structures):
            for e in stud:
                if e is not None:
                    for c in struct:
                        c.add_student(e)
        for i in range(len(self.struct_registered)):
            for e in self.struct_registered[i]:
                if e is not None:
                    for c in self.structures:
                        pass

    def new_ls(self) -> None:
        """Algorithme glouton gérant le placement des groupes

        Raises:
            Exception: Exception si erreur dans les structures
        """
        if len(self.struct_registered) == 1:
            self.place_students()
        else:
            self.registered.sort(key=lambda e: (e.index_group, e.id_for_search))
            groups = [e.index_group for e in self.registered]
            new_struct = [[] for _ in self.struct_group_min]
            tailles_structs = list(self.struct_group_min)
            g = Counter(groups)
            sizes = list(self.struct_group_min)
            sizes.sort(reverse=True)
            a_placer = []
            for ig, nb in g.most_common():
                if nb in sizes:
                    i = sizes.index(nb)
                    n_struct = tailles_structs.index(nb)
                    new_struct[n_struct].append((ig, nb))
                    tailles_structs[n_struct] -= nb
                    sizes[i] -= nb
                    sizes.sort(reverse=True)
                else:
                    a_placer.append((ig, nb))
            a_couper = []
            for ig, nb in a_placer:
                index = next(
                    (i for i, x in enumerate(tailles_structs) if x >= nb), None
                )
                if index is None:
                    a_couper.append((ig, nb))
                else:
                    new_struct[index].append((ig, nb))
                    tailles_structs[index] -= nb
            if len(a_couper) != 0:
                tailles_restantes = [t for t in tailles_structs if t != 0]
                size = pgcdn(tailles_restantes + [nb for _, nb in a_couper])
                new_studs = []
                for ig, nb in a_couper:
                    for i in range(int(nb / size)):
                        new_studs.append((ig, size))
                for ig, nb in new_studs:
                    index = next(
                        (i for i, x in enumerate(tailles_structs) if x >= nb), None
                    )
                    new_struct[index].append((ig, nb))
                    tailles_structs[index] -= nb
            for etud in self.registered:
                num_stru = -1
                for i, stru in enumerate(new_struct):
                    for j, ignb in enumerate(stru):
                        if (
                            ignb[0] == etud.index_group
                            and ignb[1] > 0
                            and num_stru == -1
                        ):
                            num_stru = i
                            new_struct[i][j] = (ignb[0], ignb[1] - 1)
                if num_stru == -1:
                    raise Exception("problem")
                index = self.struct_registered[num_stru].index(None)
                self.struct_registered[num_stru][index] = etud
        self.place_registered()

    def score_ls(self, etat: list) -> float:
        """Calcul du score

        Args:
            etat (list): état courant

        Returns:
            float: score
        """
        # delta = [etat[i].count(None) for i in range(len(etat))]
        # print('delta', delta)
        # print('mean delta', statistics.fmean(delta))
        # print('stdev delta', statistics.stdev(delta))
        # print('median delta', statistics.median(delta))
        nb_diff = [len(set(e)) for e in etat]
        # print('nb_diff', nb_diff)
        return sum(nb_diff)  # + statistics.stdev(delta)

    def voisins(self, etat: list) -> Generator[list, None, None]:
        """Génération des voisins

        Args:
            etat (list): état courant

        Yields:
            Generator[list, None, None]: liste des voisins
        """
        # voisins = []
        for s in range(len(etat)):
            for e in range(len(etat[s])):
                for ss in range(len(etat)):
                    for ee in range(len(etat[ss])):
                        if s != ss:
                            if not (etat[s][e] is None and etat[ss][ee] is None):
                                new_v = [cop[::] for cop in etat]
                                new_v[s][e], new_v[ss][ee] = new_v[ss][ee], new_v[s][e]
                                yield new_v
                                # voisins.append(new_v)
        # print(voisins)
        # return voisins

    def voisins_stud(self, student: Student) -> Generator[list, None, None]:
        """Génération des voisins selon un étudiant

        Args:
            student (Student): étudiant concerné

        Yields:
            Generator[list, None, None]: ensemble des voisins de l'étudiant
        """
        struct = -1
        pos = -1
        for i, s in enumerate(self.struct_registered):
            if student in s:
                struct = i
                pos = s.index(student)
        for i, s in enumerate(self.struct_registered):
            if i != struct:
                for j in range(len(s)):
                    new_v = [cop[::] for cop in self.struct_registered]
                    new_v[struct][pos], new_v[i][j] = new_v[i][j], new_v[struct][pos]
                    yield new_v

    def ls_group(self) -> None:
        """Début de recherche locale pour le placement des étudiants
        (ne fonctionne pas)
        """
        if len(self.structures) == 1:
            return
        # initial
        current = self.struct_registered
        # for s in self.struct_registered:
        #     cur = []
        #     for e in s:
        #         if e is not None:
        #             cur.append(e.index_group)
        #         else:
        #             cur.append(None)
        score = self.score_ls(current)
        turn = 0
        while turn < 20:
            print("turn", turn, score)
            turn += 1
            # regarde le voisinage
            # voisins = self.voisins(current)
            # recupere les notes
            scores = [(self.score_ls(v), v) for v in self.voisins(current)]
            # print(scores)
            scores.sort(key=lambda x: x[0])
            # print(scores)
            # choisi l'une des meilleures pour etre le current
            choix = 0
            score, current = scores[choix]
        self.struct_registered = current
        print(self.struct_registered)

    def add_student_to_rand_struct(self, student) -> None:
        """Ajout de l'étudiant dans une structure aléatoire

        Args:
            student ([type]): étudiant à ajouter
        """
        struct = random.randint(0, len(self.structures) - 1)
        for cla in self.structures[struct]:
            cla.add_student(student)
        self.struct_registered[struct].append(student)

    def add_student_to_struct(self, student: Student, struct: int) -> None:
        """Ajout d'un étudiant à une structure

        Args:
            student (Student): étudiant à ajouter
            struct (int): structure concernée
        """
        for cla in self.structures[struct]:
            cla.add_student(student)
        self.struct_registered[struct].append(student)

    def add_student_to_first_struct(self, student: Student) -> None:
        """Ajout d'un étudiant à la première structure libre

        Args:
            student (Student): étudiant à traiter
        """
        struct = 0
        while self.struct_group_min[struct] <= len(self.struct_registered[struct]):
            struct += 1
        for cla in self.structures[struct]:
            cla.add_student(student)
        self.struct_registered[struct].append(student)

    def remove_student_struct(self, student: Student) -> None:
        """Suppression d'un étudiant de sa structure

        Args:
            student (Student): étudiant à traiter

        Raises:
            Exception: Étudiant non inscrit
        """
        struct = -1
        for i in range(len(self.structures)):
            if student in self.struct_registered[i]:
                struct = i
        if struct == -1:
            raise Exception("erreur l etudiant n est pas inscrit a ce cours")
        self.struct_registered[struct].remove(student)
        for cla in self.structures[struct]:
            cla.remove_student(student)

    def get_diff_structures(self) -> None:
        """Génération des structures

        Raises:
            Exception: séance non présente dans une structure
            Exception: séance avec une limite de 0
        """
        to_check_after = []
        for _, conf in self:
            diff_possibilities = [[c for c_id, c in sub] for sub_id, sub in conf]
            ranges = [range(len(liste)) for liste in diff_possibilities]
            for possib in list(itertools.product(*ranges)):
                possib_obj = [diff_possibilities[i][a] for i, a in enumerate(possib)]
                effectif_min = min(possib_obj, key=lambda x: x.limit)
                valid_parents = True
                for c in possib_obj:
                    if c.parent:
                        if c.parent not in possib_obj:
                            valid_parents = False
                if not valid_parents:
                    continue
                valid = 1
                for c in possib_obj:
                    if c.nb_registered >= c.limit:
                        valid = -1
                    elif c.limit - c.nb_registered < effectif_min.limit and valid != -1:
                        valid = 0
                if valid == -1:
                    continue
                if valid == 1:
                    self.structures.append(possib_obj)
                    self.struct_group_min.append(effectif_min.limit)
                    for c in possib_obj:
                        c.nb_registered += effectif_min.limit
                elif valid == 0:
                    to_check_after.append(possib_obj)
            for possib_obj in to_check_after:
                effectif_min = min(possib_obj, key=lambda x: x.limit)
                valid = 1
                for c in possib_obj:
                    if c.nb_registered >= c.limit:
                        valid = -1
                    elif c.limit - c.nb_registered < effectif_min.limit and valid != -1:
                        valid = 0
                if valid == 0:
                    self.structures.append(possib_obj)
                    nb_places_dispo = effectif_min.limit
                    for c in possib_obj:
                        nb_places_dispo = min(
                            c.limit - c.nb_registered, nb_places_dispo
                        )
                    self.struct_group_min.append(nb_places_dispo)
                    for c in possib_obj:
                        c.nb_registered += nb_places_dispo
        classes_from_structs = [c for stru in self.structures for c in stru]
        for _, conf in self:
            for _, sub in conf:
                for _, c in sub:
                    if c not in classes_from_structs:
                        # print(self)
                        pprint.pprint(self.structures)
                        if c.limit != 0:
                            raise Exception(f"Error : {c} not in structures")
                        raise Exception(f"Error : 0 limit for class {c}")

    def str_structures(self) -> str:
        """Affichage des structures

        Returns:
            str: texte à afficher
        """
        s = ""
        for i, stru in enumerate(self.structures):
            s += f"\n\tStructure {i + 1} [{self.struct_group_min[i]}] : "
            for c in stru:
                s += f"\n\t\t - {c.id} [{c.limit}]"
        return s


class Optimization:
    """Représentation des poids du problème"""

    def __init__(self, time: int, room: int, distribution: int, student: int):
        """Constructeur des poids

        Args:
            time (int): poids des créneaux
            room (int): poids des salles
            distribution (int): poids des contraintes de répartition
            student (int): poids des étudiants
        """
        self.time = time
        self.room = room
        self.distribution = distribution
        self.student = student

    def __repr__(self) -> str:
        """Représentation des poids

        Returns:
            str: texte à afficher
        """
        s = (
            f"Optimization : time : {self.time}, "
            f"room : {self.room}, "
            f"distribution : {self.distribution}, "
            f"student : {self.student}\n"
        )
        return s


class Problem:
    """Représentation du problème"""

    def __init__(
        self, name: str, nrDays: int, nrWeeks: int, slotsPerDay: int, file_name: str
    ):
        """Constructeur du problème

        Args:
            name (str): nom du problème
            nrDays (int): nombre de jours par semaine
            nrWeeks (int): nombre de semaines
            slotsPerDay (int): nombre de créneaux par jour
            file_name (str): nom du fichier
        """
        self.name = name
        self.nrDays = nrDays
        self.nrWeeks = nrWeeks
        self.slotsPerDay = slotsPerDay
        self.nb_slots = nrDays * nrWeeks * slotsPerDay
        self.file_name = file_name

    def __repr__(self) -> str:
        """Représentation du problème

        Returns:
            str: texte à afficher
        """
        s = (
            f"Problem : name : {self.name},"
            f" nrDays : {self.nrDays}, "
            f"nrWeeks {self.nrWeeks}"
            f", slotPerDay : {self.slotsPerDay}\n"
        )
        return s
