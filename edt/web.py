"""
Gestion de l'application web depuis python
"""
from os import system

from selenium import webdriver

from edt.instance import Instance


def prepare_for_web(ins: Instance):
    """Transforme les fichiers pour les rendre lisibles pour l'appli web

    Args:
        ins (Instance): l'instance a convertir
    """
    prob_file = ins.problem.file_name
    sol_file = f"ressources/solutions/solution_{ins.problem.name}.xml"
    prb_file = "theleme/web/web/dist/stageproject/app/XML/problem.xml"
    sln_file = "theleme/web/web/dist/stageproject/app/XML/solution.xml"
    system(f"cp {prob_file} {prb_file}")
    system(f"cp {sol_file} {sln_file}")
    script = "theleme/scripts/creneaux_to_min.sh"
    system(f"./{script} {prb_file} {prb_file}_tmp")
    system(f"./{script} {sln_file} {sln_file}_tmp")

    system(f"mv {prb_file}_tmp {prb_file}")
    system(f"mv {sln_file}_tmp {sln_file}")


def launch_on_web(ins: Instance):
    """Ouvre le site dans Firefox et prépare la date

    Args:
        ins (Instance): l'instance utilisée
    """
    ins.get_xml_solution()
    prepare_for_web(ins)

    print("web")
    driver = webdriver.Firefox()
    driver.get(url="http://localhost/edt/dist/stageproject/")

    id_box = driver.find_element_by_class_name("btn-dark")
    id_div = driver.find_element_by_xpath("//input")
    id_div.send_keys("2020-08-24")
    id_box.click()
