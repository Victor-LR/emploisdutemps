"""
Gestion de la recherche en programmation par contraintes
"""
import datetime
import enum
import random

# import time
from os import system
from typing import List, Tuple, Optional, Union, Dict

import minizinc

from edt.instance import Instance, Score

# from termcolor import colored

distribution = enum.Enum(
    "distribution",
    [
        "same_start",
        "same_time",
        "same_days",
        "same_weeks",
        "same_room",
        "different_time",
        "different_days",
        "different_weeks",
        "different_room",
        "overlap",
        "same_attendees",
        "precedence",
        "work_day",
        "min_gap",
        "not_overlap",
        "max_days",
        "max_day_load",
        "max_breaks",
        "max_block",
    ],
)


def randomize_possibilities(ins: Instance) -> None:
    """Bloque certaines possibilités de salles et créneaux

    Args:
        ins (Instance): instance à traiter
    """
    for c in ins.all_classes:
        if random.randint(0, 10) > 1:
            c.times_for_search = [t for t in c.times if t.penalty <= 4]
            # random.sample(c.times, min(len(c.times), 2))
        else:
            if len(c.times_for_search) != 1:
                c.times_for_search = [
                    random.choice([t for t in c.times if t.penalty <= 1])
                ]


def create_dzn_file(ins: Instance, light: bool, xclass: list = None) -> None:
    """Création d'un fichier dzn

    Args:
        ins (Instance): instance à traiter
        light (bool): True si student sectioning pendant la recherche
        xclass (list, optional): liste des inscriptions aux séances. Defaults to None.
    """
    with open(f"mzn_files/dzn/{ins.problem.name}.dzn", "w") as f:
        for n, v in ins.mzn_inst.items():
            f.write(str(n))
            f.write("=")
            if n == "room_penalty":
                f.write(
                    f"array2d(1..{len(ins.all_classes)}"
                    f", 1..{len(ins.rooms)},"
                    f"{[e for l in v for e in l]})"
                )
            elif n == "travel":
                f.write(
                    f"array2d(1..{len(ins.rooms)}"
                    f", 1..{len(ins.rooms)},"
                    f"{[t for e in v for t in e]})"
                )
            elif n == "distribution":
                f.write("[")
                for e in v:
                    f.write(e)
                    f.write(", ")
                f.write("]")
            elif n == "distribution_classes":
                table = [1 for _ in ins.distributions]
                if light:
                    table += [1 for _ in ins.same_attends]
                f.write(
                    f"array2d(1..{len(table)}"
                    f", 1..{len(ins.all_classes)},"
                    f"{[t for e in v for t in e]})"
                )
            else:
                f.write(str(v))
            f.write(";\n")
        if xclass:
            f.write(
                f"pre_xclass = array2d(1..{len(ins.students)}"
                f", 1..{len(ins.all_subparts)},"
                f"{[t for e in xclass for t in e]});\n"
            )
    system(
        "sed 's/set()/{}/g' mzn_files/dzn/"
        + ins.problem.name
        + ".dzn > mzn_files/dzn/"
        + ins.problem.name
        + "tmp.dzn"
    )
    system(
        "mv mzn_files/dzn/"
        + ins.problem.name
        + "tmp.dzn "
        + "mzn_files/dzn/"
        + ins.problem.name
        + ".dzn"
    )


def launch_instance_on_mzn(
    model_file: str = "mzn_files/edt.mzn",
    ins: Instance = None,
    solve: str = "all",
    xclass: list = None,
    light: bool = False,
) -> Tuple[object, Optional[Union[List[Dict], Dict]], Dict]:
    """Lancement de la recherche en minizinc

    Args:
        model_file (str, optional): fichier du modèle. Defaults to "mzn_files/edt.mzn".
        ins (Instance, optional): instance à traiter. Defaults to None.
        solve (str, optional): type de résolution. Defaults to "all".
        xclass (list, optional): liste des inscriptions aux séances. Defaults to None.
        light (bool, optional): True si student sectioning avant la recherche.
        Defaults to False.

    Returns:
        Tuple[object, Optional[Union[List[Dict], Dict]], Dict]: résultats
    """
    edt_model = minizinc.Model(model_file)
    if xclass is not None:
        edt_model.add_string(
            """
            array[STUDENTS,SUBPARTS] of CLASSES_P: pre_xclass;
            constraint x_class = pre_xclass;
            """
        )
    options_light = {
        "all": """
        solve
         :: seq_search([
           int_search(x_w_start, input_order, indomain_min),
           %int_search(x_time, input_order, indomain_min),
           %int_search(x_room, first_fail, indomain_random),
          ])
          satisfy;
        """,
        "less_cost": f"constraint x_cost < {int(ins.last_cost * 0.90)};"
        "solve satisfy;",
    }
    options_n = {
        "all": """
                    solve
                     :: seq_search([
                    %   int_search(x_headcount, input_order, indomain_max),
                       int_search(x_class, first_fail, indomain_min),
                       int_search(x_w_start, input_order, indomain_min),
                    %   int_search(x_room, first_fail, indomain_random),
                    %   int_search(x_time, input_order, indomain_min),
                      ])
                      satisfy;
                """,
        "less_cost": f"constraint x_cost < {int(ins.last_cost * 0.90)};"
        "solve satisfy;",
    }
    options = options_light if light else options_n
    edt_model.add_string(options[solve])
    gecode = minizinc.Solver.lookup("gecode")
    # create_dzn_file(ins=ins, xclass=xclass, light=light)
    instance = minizinc.Instance(gecode, edt_model)
    for n, v in ins.mzn_inst.items():
        if n == "distribution":
            instance["DISTRIBUTION"] = distribution
            instance[n] = [distribution[d] for d in v]
        elif n != "":
            instance[n] = v
    if xclass:
        instance["pre_xclass"] = xclass
    # print(datetime.datetime.now())
    # start_time = time.time()
    solutions = instance.solve(datetime.timedelta(seconds=30))
    # print("--- %s seconds ---" % (time.time() - start_time))
    return solutions


def print_result(
    result: Tuple[object, Optional[Union[List[Dict], Dict]], Dict], light: bool = False
) -> None:
    """Affichage des résultats de minizinc

    Args:
        result (Tuple[object, Optional[Union[List[Dict], Dict]], Dict]): résultats
        light (bool, optional): True si student sectioning avant la recherche.
        Defaults to False.
    """
    print(result)
    if not result.solution:
        print("insatisfiable")
    else:
        print("-" * 20)
        print("x_time : ", result["x_time"])
        print("x_room : ", result["x_room"])
        if not light:
            print("x_class : ")
            for ligne in result["x_class"]:
                print(ligne)
            print("x_headcount : ", result["x_headcount"])
        print("x_p_time : ", result["x_p_time"])
        print("x_p_room : ", result["x_p_room"])
        print("x_p_student : ", result["x_p_student"])
        print("x_p_distribution : ", result["x_p_distribution"])
        print("x_cost : ", result["x_cost"])
        print("x_t_time : ", result["x_t_time"])
        print("x_t_room : ", result["x_t_room"])
        print("x_t_student : ", result["x_t_student"])
        print("x_t_distribution : ", result["x_t_distribution"])
        print("x_w_start : ", result["x_w_start"])
        print("x_ec_duration : ", result["x_ec_duration"])
        print("x_rc_duration : ", result["x_rc_duration"])
        try:
            print("objective : ", result["objective"])
        except KeyError:
            pass
    print("statistics : ", result.statistics)


def get_times_and_rooms(
    result: Tuple[object, Optional[Union[List[Dict], Dict]], Dict],
    ins: Instance,
    light: bool = False,
) -> None:
    """Place les créneaux et salles de classes après la recherche

    Args:
        result (Tuple[object, Optional[Union[List[Dict], Dict]], Dict]): résultats de
        la recherche minizinc
        ins (Instance): instance à traiter
        light (bool, optional): True si student sectioning avant la recherche.
        Defaults to False.
    """
    if result.solution:
        for i, t in enumerate(result["x_time"]):
            ins.all_classes[i].time = ins.all_times[t - 1]
            if not light:
                ins.all_classes[i].registered = []
        for i, ri in enumerate(result["x_room"]):
            real_class = ins.all_classes[i]
            for r in real_class.classrooms:
                if r[0].int_id == ins.rooms[ins.all_rooms[ri - 1]].int_id:
                    real_class.room = r
        if not light:
            for i, ic in enumerate(result["x_class"]):
                ins.all_students[i].classes = []
                for c in ic:
                    if c != 0:
                        ins.all_classes[c - 1].add_student(ins.all_students[i])
            ins.xclass = result["x_class"]
        ins.last_cost = result["x_cost"]
    # else:
    #     print('insatisfliable')


def freeze(ins: Instance) -> None:
    """Blocage des séances

    Args:
        ins (Instance): instance à traiter
    """
    for c in ins.all_classes:
        if random.random() < 0.50:
            c.times_for_search = c.times
        else:
            c.times_for_search = [c.time]
        if random.random() < 0.50:
            c.rooms_for_search = c.classrooms
        else:
            c.rooms_for_search = [c.room]


def condition_fin(scores: List[Score]) -> bool:
    """Condition d'arrêt de la LNS

    Args:
        scores (List[Score]): liste des scores

    Returns:
        bool: True si fin de la recherche
    """
    return len(scores) >= 200 or len(scores) > 20 and len(set(scores[-10:])) == 1


def lns(ins: Instance, light: bool = False) -> List[Score]:
    """Recherche avec la LNS

    Args:
        ins (Instance): instance à traiter
        light (bool, optional): True si student sectioning avant la recherche.
        Defaults to False.

    Returns:
        List[Score]: scores obtenus pendant la recherche
    """
    scores = []
    launch_mzn(ins, "all", light=light)
    scores.append(ins.get_score())
    while not condition_fin(scores):
        freeze(ins)
        launch_mzn(
            ins, solve="less_cost", xclass=ins.xclass, verbose=False, light=light
        )
        scores.append(ins.get_score())
        # print(f'score[{len(scores) - 1}] = {scores[len(scores) - 1]}')
    return scores


def launch_mzn(
    ins: Instance, solve="all", xclass: list = None, verbose=False, light=False
) -> bool:
    """Lancement du modèle minizinc

    Args:
        ins (Instance): instance à traiter
        solve (str, optional): paramètre de résolution. Defaults to "all".
        xclass (list, optional): salles de classes. Defaults to None.
        verbose (bool, optional): True si afficher les résultats. Defaults to False.
        light (bool, optional): True si student sectioning avant recherche.
        Defaults to False.

    Returns:
        bool: True si la recherche trouve une solution
    """
    if light:
        xclass = None
    ins.generate_instance_mzn(light=light)
    result = launch_instance_on_mzn(
        model_file="mzn_files/model_edt_light.mzn"
        if light
        else "mzn_files/model_edt.mzn",
        ins=ins,
        solve=solve,
        xclass=xclass,
        light=light,
    )
    if result.solution:
        get_times_and_rooms(result, ins, light)
        if verbose:
            print_result(result, light)
        # print(ins.get_score(verbose=True))
        return True
    # print(colored('insatisfiable', on_color='on_red'))
    return False
