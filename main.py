import datetime
import time

from edt.instance import get_instance
from edt.local_search import local_search

from edt.ppc import launch_mzn, lns

import sys

print(sys.version)

recuit = True
light = False
method = "ppc"  # 'ppc' 'lns' 'ls'
rand_init = False  # initialisation aleatoire ou tabou
web = False
show_groups = False
start_time = time.time()

# import random
# random.seed(4)

path = "ressources/datasetITC2019/"

# file = 'tg-fal17.xml'  # no students
# file = 'tg-spr18.xml'  # no students
# file = 'iku-spr18.xml'  # no students
# file = 'lums-spr18.xml'  # no students
# file = 'lums-fal17.xml'  # no students
# file = 'bet-sum18.xml'  # no students
# file = 'lums-sum17.xml'  # no students
# file = 'iku-fal17.xml'  # no students

# file = "agh-h-spr17.xml"  # class 99 limit
# file = "agh-fis-spr17.xml"  # class 154 limit
# file = "pu-d5-spr17.xml"  # class 1039 et 1040
# file = "muni-fi-fal17.xml"  # class 80 limit
# file = "muni-pdf-spr16c.xml"  # 0 limit
# file = "mary-fal18.xml"  # 0 limit
# file = "agh-fal17.xml"  # 0 limit
# file = "pu-c8-spr07.xml"  # 0 limit
# file = "mary-spr17.xml"  # 0 limit
# file = "bet-spr18.xml"  # 0 limit
# file = "pu-proj-fal19.xml"  # 0 limit
# file = "bet-fal17.xml"  # 0 limit
# file = "pu-d9-fal19.xml"  # 0 limit
# file = "pu-llr-spr17.xml"  # 0 limit
# file = "muni-fi-spr16.xml"  # 0 limit
# file = "muni-fsps-spr17.xml"  # 0 limit
# file = "pu-llr-spr07.xml"  # 0 limit

# file = "nbi-spr18.xml"
# file = "muni-fspsx-fal17.xml"
# file = "agh-ggis-spr17.xml"  # 47 000 (min)
# file = "muni-fi-spr17.xml"
# file = "wbg-fal10.xml"
# file = "muni-pdfx-fal17.xml"
# file = "yach-fal17.xml"
file = "muni-fsps-spr17c.xml"  # proche UA
# file = "pu-cs-fal07.xml"
# file = "muni-pdf-spr16.xml"
# file = "agh-ggos-spr17.xml"
file = path + file

file = "ressources/L3_instances/week10-room5-dist1.xml"

print(file)
print(datetime.datetime.now().strftime("%H:%M:%S"))
ins = get_instance(file, light)

if show_groups:
    from edt.student_sectionning import plot_rencontres

    plot_rencontres(ins, "Sectionning with greedy algorithm")

if method == "ls":
    print("recherche locale")
    scores = local_search(ins, rand_init=rand_init, recuit=recuit)
    print(scores[-1])
elif method == "lns":
    if light:
        print("lns après student sectionning")
    else:
        print("lns après student sectionning dans ppc")
    scores = lns(ins, light=light)
elif method == "ppc":
    if light:
        print("ppc apres student sectionning")
    else:
        print("ppc avec student sectionning")
    launch_mzn(ins=ins, solve="all", light=light)
    ins.get_xml_solution()

t = time.time() - start_time
h = t // 3600
s = t - (h * 3600)
m = s // 60
s = s - (m * 60)
print(f"Execution time : {int(h):02}:{int(m):02}:{int(s):02}")
