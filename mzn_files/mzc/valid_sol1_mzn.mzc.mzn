
%% DIMENSIONS CONSTANTES
int: nr_criteria = 4;


%% DIMENSIONS LUES EN ENTREE
int: nr_weeks; % nombre de semaines sur l'horizon de temps
int: nr_days_per_week; % nombre de jours par semaine
int: nr_slots_per_day; % nombre de créneaux par jour
int: nr_breaks; % le nombre de types de pauses
int: nr_courses; % nombre total d'UE répertoriées dans le problème
int: nr_configs; % nombre total de configurations répertoriées
int: nr_subparts; % nombre total de séances-type répertoriées
int: nr_classes; % nombre total de classes répertoriées
int: nr_times; % nombre total de séances répertoriées
int: nr_rooms; % nombre total de salles
int: max_capacity; % capacité maximale de salle recensée
int: nr_closures; % nombre total de fermetures de salles répertoriées
int: nr_students; % nombre total d'étudiants
int: nr_distributions; % nombre total de contraintes de distribution


%% DIMENSIONS AUXILLIAIRES
int: nr_slots = nr_weeks * nr_days_per_week * nr_slots_per_day; % rang du dernier créneau de l'horizon de temps
int: max_unreachables = nr_rooms*nr_rooms; % taille de la matrice d'inatteignabilité
int: cst_room_base = pow(10,ceil(log(10,nr_rooms))); % base pour l'encodage des paires de salles

%% ASSERTIONS
constraint assert(nr_weeks > 0, "Invalid datafile: " ++  "Number of weeks should be strictly positive");
constraint assert(nr_days_per_week in 1..7, "Invalid datafile: " ++  "Number of days per week should belong to 1..7");
constraint assert(nr_slots_per_day in 1..144, "Invalid datafile: " ++  "Number of slots per days should belong to 1..24*6, ie, the minimum granularity allowed is 10 minutes each slot");
constraint assert(nr_slots == nr_weeks * nr_days_per_week * nr_slots_per_day, "Invalid datafile: " ++  "Incorrect number of slots");
constraint assert(nr_breaks > 0, "Invalid datafile: " ++  "Number of break types should be strictly positive");
constraint assert(nr_courses > 0, "Invalid datafile: " ++  "Number of courses should be strictly positive");
constraint assert(nr_configs >= nr_courses, "Invalid datafile: " ++  "Number of configurations should be greater than or equal to the number of courses");
constraint assert(nr_subparts >= nr_configs, "Invalid datafile: " ++  "Number of subparts should be greater than or equal to the number of configurations");
constraint assert(nr_classes >= nr_subparts, "Invalid datafile: " ++  "Number of classes should be greater than or equal to the number of subparts");
constraint assert(nr_times >= nr_classes, "Invalid datafile: " ++  "Number of times should be greater than or equal to the number of classes");
constraint assert(nr_rooms > 0, "Invalid datafile: " ++  "Number of rooms should be strictly positive");
constraint assert(nr_students > 0, "Invalid datafile: " ++  "Number of students should be strictly positive");
% NB. Pas d'assertion sur capacité maximum, nombre de fermetures ou d'inatteignables


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PLAGES/ENSEMBLES

%% PLAGES CONSTANTES (INSTANCE-INDEPENDANTES)
enum CONSTRAINT_TYPE = {hard,soft}; % type des contraintes : dure ou souple
enum DISTRIBUTION = {same_start, same_time, same_days, same_weeks, same_room, different_time, different_days, different_weeks, different_room, overlap, same_attendees, precedence, work_day, min_gap, not_overlap, max_days, max_day_load, max_breaks, max_block}; % identfiants des prédicats de distribution
enum CRITERION = {k_time, k_room, k_student, k_distribution}; % critères de coût


%% PLAGES FONDAMENTALES
set of int: WEEKS = 1..nr_weeks;
set of int: WEEKDAYS = 1..nr_days_per_week;
set of int: DAYSLOTS = 1..nr_slots_per_day;
set of int: SLOTS = 1..nr_slots;
set of int: BREAKS = 1..nr_breaks;
set of int: COURSES = 1..nr_courses;
set of int: CONFIGS = 1..nr_configs;
set of int: SUBPARTS = 1..nr_subparts;
set of int: CLASSES = 1..nr_classes;
set of int: HEADCOUNT = 0..5000; %TODO : borne sup = card(STUDENTS);
set of int: TIMES = 1..nr_times;
set of int: ROOMS = 1..nr_rooms;
set of int: CAPACITY = 0..max_capacity;
set of int: CLOSURES = 1..nr_closures;
set of int: STUDENTS = 1..nr_students;
set of int: DISTRIBUTIONS = 1..nr_distributions;
set of int: PENALTY = 1..100; %TODO borne sup = pénalité max recensée + ajouter une plage cost (sa borne sup peut être calculée)
set of int: CRITERIA = 1..nr_criteria;
set of int: WEIGHTS = 0..10; % TODO devrait être un ensemble de 4 entiers (plus 0) afin de pouvoir imposer n'importe quel ordre lexicographique entres les 4 critères à l'aide des poids


%% PLAGES AUXILLIAIRES
% On ajoute un *élément nul* (modélisant un objet fictif) à certaines dimensions afin de réifier la non-sélection de composant dans la *configuration* de l'EDT solution (eg. classe fictive choisie pour une UE fermée, séance fictive affectée à une classe fermée, salle fictive - et non pas *virtuelle* - allouée à une classe fermée).
%  ! la valeur adoptée pour l'élément nul d'une dimension peut simplifier (ou non !) l'expression de contraintes de configuration.
int: cst_null_break = 0; % type de pause nul pour deux salles toujours atteignables
set of int: BREAKS_P = {cst_null_break} union BREAKS;
int: cst_null_config = 0; % configuration nulle assignée à une UE fermée
set of int: CONFIGS_P = {cst_null_config} union CONFIGS;
int: cst_null_class = 0; % classe nulle assignée à une séance-type fermée en cas de configuration fermée
set of int: CLASSES_P = {cst_null_class} union CLASSES;
int: cst_null_time = 0; % séance nulle assignée à une classe fermée
set of int: TIMES_P = {cst_null_time} union TIMES;
int: cst_null_room = 0; % salle nulle allouée à une classe fermée
set of int: ROOMS_P = {cst_null_room} union ROOMS;
set of int: ROOM_PAIRS = 0..(cst_room_base+1)*nr_rooms; % plage d'encodage de paires de salles
int: cst_null_penalty = 0; % pénalité nulle : affectée aux salles non-pré-allouées, affectée aux contraintes dures de distribution (*required*)
set of int: PENALTY_P = {cst_null_penalty} union PENALTY;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STRUCTURES DE DONNEES


%% STRUCTURES LUES EN ENTREE
array[COURSES] of set of CONFIGS: configs; % les configurations des cours
array[CONFIGS] of set of SUBPARTS: subparts; % les séances-type des configurations
array[SUBPARTS] of set of CLASSES: classes; % les classes des séances-type
array[CLASSES] of CLASSES_P: parent; % les parents des classes (la classe nulle est attribuée aux classes sans parent)
array[CLASSES] of HEADCOUNT: limit; % la limite d'effectif des classes
array[CLASSES] of set of TIMES: times; % les séances alternatives des classes
array[TIMES] of set of WEEKS: time_weeks; % les semaines requises par séances
array[TIMES] of set of WEEKDAYS: time_days; % les journées requises par séances
array[TIMES] of DAYSLOTS: time_slot; % le créneau requis par séance
array[TIMES] of DAYSLOTS: time_length; % le nombre de créneaux successifs requis par séance %TODO ignoré pour l'instant (supposé égal à 1)
array[TIMES] of PENALTY_P: time_penalty; % la pénalité encourue par séance
array[CLASSES] of set of ROOMS: rooms; % les salles allouables par classe %TODO représenter sous forme de matrice comme pour les pénalités ?
array[CLASSES,ROOMS] of PENALTY_P: room_penalty; % la pénalité encourue par salle par classe
set of ROOMS: virtual_rooms; % l'ensemble des salles virtuelles répertoriées %TODO ne fait pas partie du XML ITC -> à "sortir" et définir dans un autre sous-modèle
array[ROOMS] of CAPACITY: capacity; % les capacités des salles
array[DAYSLOTS] of set of ROOM_PAIRS: break; % les paires ordonnées (r1,r2) de salles inatteignables par créneau journalier s : si r1 est allouée à une classe affectée à s, r2 ne peut pas être allouée à cette classe si elle est affectée à s+1 le(s) même(s) jour(s) %TODO ne fait pas partie du XML ITC -> à "sortir" et définir dans un autre sous-modèle
array[ROOMS] of set of CLOSURES: closures; % les fermetures des salles z
array[CLOSURES] of set of WEEKS: closure_weeks; % les semaines par fermeture
array[CLOSURES] of set of WEEKDAYS: closure_days; % les journées par fermeture
array[CLOSURES] of DAYSLOTS: closure_slot; % le créneau par fermeture
array[CLOSURES] of DAYSLOTS: closure_length; % le nombre de créneaux successifs par fermeture %TODO ignoré pour l'instant (supposé égal à 1)
array[STUDENTS] of set of COURSES: inscriptions; % les UE des étudiants
array[DISTRIBUTIONS] of DISTRIBUTION: distribution; % contraintes de distribution répertoriées
array[DISTRIBUTIONS] of PENALTY_P: distribution_penalty; % pénalités associées aux contraintes de distribution (avec **pénalité nulle** pour contraintes dures)
array[DISTRIBUTIONS,CLASSES] of CLASSES_P: distribution_classes; % liste des classes concernées par contrainte de distribution : *[m,c]=i si la classe `c` est la i-ème classe dans la portée de la contrainte `m`, *[m,c]==cst_null_class si la classe `c` n'est pas dans la portée de la contrainte `m`
array[CRITERIA] of WEIGHTS: weight; % poids des critères


%% STRUCTURES AUXILLIAIRES
% Datation : à tout point de l'horizon correspond un triplet unique de rangs <créneau,jour,semaine> (eg. 380=[[4,3,10]]=14h-15h20 mercredi en dixième semaine de l'horizon).
array[SLOTS] of WEEKS: week = [ ((i-1) div (nr_slots_per_day*nr_days_per_week)) + 1 | i in SLOTS]; % la semaine associée à chaque point de l'horizon
array[SLOTS] of WEEKDAYS: weekday = [ (((i-1) mod (nr_slots_per_day*nr_days_per_week)) div nr_slots_per_day) + 1 | i in SLOTS]; % la journée associée à chaque point de l'horizon
array[SLOTS] of DAYSLOTS: dayslot = [ (((i-1) mod (nr_slots_per_day*nr_days_per_week)) mod nr_slots_per_day) + 1 | i in SLOTS]; % le créneau associé à chaque point de l'horizon
% Structures inverses
array[CONFIGS] of COURSES: course = [ u | p in CONFIGS, u in COURSES where p in configs[u]]; % `configs` est le graphe de la fonction `course`
array[SUBPARTS] of CONFIGS: config = [ p | s in SUBPARTS, p in CONFIGS where s in subparts[p]]; % `subparts` est le graphe de la fonction `config`
array[CLASSES] of SUBPARTS: subpart = [ s | c in CLASSES, s in SUBPARTS where c in classes[s]]; % `classes` est le graphe de la fonction `subpart`
 array[CLASSES] of set of CLASSES: children = [ { c | c in CLASSES where parent[c]==p} | p in CLASSES]; % `children` est le graphe de la fonction `parent` privé de la classe nulle
array[TIMES] of CLASSES: class = [ c | c in CLASSES, t in TIMES where t in times[c]]; % `times` est le graphe de la fonction `time`
array[ROOMS] of set of CLASSES: room_classes = [{ c | c in CLASSES where r in rooms[c]} | r in ROOMS ]; % `room_classes` est l'inverse de `rooms`
array[TIMES, SLOTS] of 0..1: open_slots = array2d(TIMES, SLOTS,[bool2int(week[s] in time_weeks[t] /\ weekday[s] in time_days[t] /\ dayslot[s]==time_slot[t]) | t in TIMES, s in SLOTS]);
array[CLOSURES, SLOTS] of 0..1: closed_slots = array2d(CLOSURES, SLOTS, [bool2int(week[s] in closure_weeks[t] /\ weekday[s] in closure_days[t] /\ dayslot[s]==closure_slot[t]) | t in CLOSURES, s in SLOTS]);
array[ROOMS, SLOTS] of 0..1: room_closed_slots = array2d(ROOMS, SLOTS,[bool2int( s in {i | t1 in CLOSURES, i in SLOTS where closed_slots[t1,i] == 1 /\ t1 in closures[r]}) | r in ROOMS, s in SLOTS]);
array[COURSES] of set of STUDENTS: students = [{ s | s in STUDENTS where u in inscriptions[s]} | u in COURSES ]; % `students` est l'inverse de `inscriptions`
array[STUDENTS] of set of SUBPARTS: student_subpart_set = [array_union([subparts[p] | u in inscriptions[e], p in configs[u]]) | e in STUDENTS]; % ensemble des séances-type des UE auxquelles un étudiant est inscrit
array[STUDENTS] of set of CLASSES: student_class_set = [array_union([classes[s] | u in inscriptions[e], p in configs[u], s in subparts[p]]) | e in STUDENTS]; % ensemble des classes des UE auxquelles un étudiant est inscrit
array[CONSTRAINT_TYPE] of set of DISTRIBUTIONS: typed_distributions = [{m | m in DISTRIBUTIONS where distribution_penalty[m]==cst_null_penalty /\ hard==1},{m | m in DISTRIBUTIONS where distribution_penalty[m]!=cst_null_penalty /\ soft==2}]; % contraintes de distribution par type de contraintes
array[DISTRIBUTIONS] of set of CLASSES: distribution_class_set = [{c | c in CLASSES where distribution_classes[m,c]!=cst_null_class} | m in DISTRIBUTIONS]; % ensemble des classes dans la portée d'une contrainte de distribution
array[CRITERIA] of set of int: COST_RANGE = [0..c_k_time,0..c_k_room,0..c_k_student,0..c_k_distribution]; % plage de coût possible par critère par élément imputable
int: c_k_time = sum(c in CLASSES)(max(t in times[c])(time_penalty[t]));
int: c_k_room = sum(c in CLASSES)(max(r in rooms[c])(room_penalty[c,r])); % ! 0 for classes with no room assigned
int: c_k_student = sum(s in STUDENTS)(card(student_subpart_set[s]) * (card(student_subpart_set[s]) - 1) div 2);
int: c_k_distribution = sum(m in DISTRIBUTIONS where m in typed_distributions[soft] /\ m<=min_gap)(distribution_penalty[m] * card(distribution_class_set[m]) * (card(distribution_class_set[m]) - 1) div 2)
+ sum(m in DISTRIBUTIONS where m in typed_distributions[soft] /\ m==max_days)(distribution_penalty[m] * max(WEEKDAYS))
+ sum(m in DISTRIBUTIONS where m in typed_distributions[soft] /\ m>=max_day_load)(distribution_penalty[m] * max(WEEKDAYS)*card(distribution_class_set[m]));
set of int: COST = 0..sum(k in CRITERIA)(weight[k]*card(COST_RANGE[k])); % plage de la fonction coût


array[int] of int: xb_o_course;
array[int] of int: xb_c_course;
array[int] of int: xb_o_config;
array[int] of int: xb_c_config;
array[int] of int: xb_o_subpart;
array[int] of int: xb_c_subpart;
array[int] of int: xb_o_class;
array[int] of int: xb_c_class;
array[int] of int: x_time;
array[int] of int: x_room;
array[int,int] of int: x_class;
array[int] of int: x_headcount;
array[int] of int: x_p_time;
array[int] of int: x_p_room;
array[int] of int: x_p_student;
array[int] of int: x_p_distribution;
int: x_cost;


array[int] of int: xb_o_course_c = [1, 1];
array[int] of int: xb_c_course_c = [0, 0];
array[int] of int: xb_o_config_c = [1, 1];
array[int] of int: xb_c_config_c = [0, 0];
array[int] of int: xb_o_subpart_c = [1, 1, 1, 1];
array[int] of int: xb_c_subpart_c = [0, 0, 0, 0];
array[int] of int: xb_o_class_c = [1, 1, 1, 1];
array[int] of int: xb_c_class_c = [0, 0, 0, 0];
array[int] of int: x_time_c = [1, 2, 3, 4];
array[int] of int: x_room_c = [2, 3, 2, 2];
array[int,int] of int: x_class_c = array2d(1..3,1..4,[1, 2, 3, 4, 1, 2, 3, 4,
1, 2, 0, 0]);
array[int] of int: x_headcount_c = [3, 3, 2, 2];
array[int] of int: x_p_time_c = [0, 0, 0, 0];
array[int] of int: x_p_room_c = [0, 0, 0, 8];
array[int] of int: x_p_student_c = [0, 0, 0];
array[int] of int: x_p_distribution_c = [];
int: x_cost_c = 8;


test check(bool: b,string: s) =
  if b then true else trace_stdout("ERROR: "++s++"\n",false) endif;

array[int] of bool: checks = [
  check(xb_o_course_c==xb_o_course, "xb_o_course"),
  check(xb_c_course_c==xb_c_course, "xb_c_course"),
  check(xb_o_config_c==xb_o_config, "xb_o_config"),
  check(xb_c_config_c==xb_c_config, "xb_c_config"),
  check(xb_o_subpart_c==xb_o_subpart, "xb_o_subpart"),
  check(xb_c_subpart_c==xb_c_subpart, "xb_c_subpart"),
  check(xb_o_class_c==xb_o_class, "xb_o_class"),
  check(xb_c_class_c==xb_c_class, "xb_c_class"),
  check(x_time_c==x_time, "x_time"),
  check(x_room_c==x_room, "x_room"),
  check(x_class_c==x_class, "x_class"),
  check(x_headcount_c==x_headcount, "x_headcount"),
  check(x_p_time_c==x_p_time, "x_p_time"),
  check(x_p_room_c==x_p_room, "x_p_room"),
  check(x_p_student_c==x_p_student, "x_p_student"),
  check(x_p_distribution_c==x_p_distribution, "x_p_distribution"),
  check(x_cost_c==x_cost, "x_cost_c")
];

output [
  if forall(checks)
  then "CORRECT\n"
  else "INCORRECT\n"
  endif
];
